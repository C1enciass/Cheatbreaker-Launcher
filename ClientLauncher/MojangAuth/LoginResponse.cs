﻿using System;
using System.Collections.Generic;
using ClientLauncher.MojangAuth.Responses;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x0200003F RID: 63
	internal class LoginResponse : Response
	{
		// Token: 0x060001E5 RID: 485 RVA: 0x00008234 File Offset: 0x00006434
		public bool IsValidAccount()
		{
			return this.accessToken != null && this.user != null && !string.IsNullOrEmpty(this.user.id);
		}

		// Token: 0x040000EB RID: 235
		public string accessToken;

		// Token: 0x040000EC RID: 236
		public string clientToken;

		// Token: 0x040000ED RID: 237
		public List<LoginProfile> availableProfiles;

		// Token: 0x040000EE RID: 238
		public LoginProfile selectedProfile;

		// Token: 0x040000EF RID: 239
		public UserProfile user;
	}
}
