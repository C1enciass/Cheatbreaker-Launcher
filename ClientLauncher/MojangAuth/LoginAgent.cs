﻿using System;

namespace ClientLauncher.MojangAuth
{
	// Token: 0x02000037 RID: 55
	public class LoginAgent
	{
		// Token: 0x060001C9 RID: 457 RVA: 0x00007934 File Offset: 0x00005B34
		public LoginAgent()
		{
			this.name = "Minecraft";
			this.version = "1";
		}

		// Token: 0x040000CD RID: 205
		public string name;

		// Token: 0x040000CE RID: 206
		public string version;
	}
}
