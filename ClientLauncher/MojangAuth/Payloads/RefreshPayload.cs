﻿using System;

namespace ClientLauncher.MojangAuth.Payloads
{
	// Token: 0x02000045 RID: 69
	internal class RefreshPayload : Payload
	{
		// Token: 0x060001EC RID: 492 RVA: 0x00008263 File Offset: 0x00006463
		public RefreshPayload(string accessToken, string clientToken)
		{
			this.accessToken = accessToken;
			this.clientToken = clientToken;
		}

		// Token: 0x040000F4 RID: 244
		public string accessToken;

		// Token: 0x040000F5 RID: 245
		public string clientToken;
	}
}
