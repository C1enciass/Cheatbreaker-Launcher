﻿using System;

namespace ClientLauncher.MojangAuth.Responses
{
	// Token: 0x02000042 RID: 66
	internal class RefreshResponse : Response
	{
		// Token: 0x040000F2 RID: 242
		public string accessToken;
	}
}
