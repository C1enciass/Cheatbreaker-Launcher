﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000010 RID: 16
	public class MinecraftPingResult
	{
		// Token: 0x0400002D RID: 45
		public string host = string.Empty;

		// Token: 0x0400002E RID: 46
		public int port;

		// Token: 0x0400002F RID: 47
		public bool success;

		// Token: 0x04000030 RID: 48
		public int numPlayers;

		// Token: 0x04000031 RID: 49
		public int maxPlayers;
	}
}
