﻿using System;
using System.Windows;

namespace ClientLauncher
{
	// Token: 0x0200002B RID: 43
	public class WindowManager
	{
		// Token: 0x06000162 RID: 354 RVA: 0x00006905 File Offset: 0x00004B05
		public static void Init()
		{
			Application.Current.MainWindow = WindowManager._mainWindow;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00006918 File Offset: 0x00004B18
		public static void Shutdown()
		{
			WindowManager.EndCurrentWindow();
			if (WindowManager._mainWindow != null)
			{
				try
				{
					WindowManager._mainWindow.Close();
				}
				catch
				{
				}
				finally
				{
					WindowManager._mainWindow = null;
				}
			}
			Application.Current.MainWindow = null;
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006970 File Offset: 0x00004B70
		public static MainWindow GetMainWindow()
		{
			return WindowManager._mainWindow;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00006978 File Offset: 0x00004B78
		public static void EndCurrentWindow()
		{
			if (WindowManager._eulaWindow != null)
			{
				try
				{
					WindowManager._eulaWindow.Close();
				}
				catch
				{
				}
				finally
				{
					WindowManager._eulaWindow = null;
				}
			}
			if (WindowManager._loadingSplash != null)
			{
				try
				{
					WindowManager._loadingSplash.Close();
				}
				catch
				{
				}
				finally
				{
					WindowManager._loadingSplash = null;
				}
			}
			if (WindowManager._selfAutoUpdateWindow != null)
			{
				try
				{
					WindowManager._selfAutoUpdateWindow.Close();
				}
				catch
				{
				}
				finally
				{
					WindowManager._selfAutoUpdateWindow = null;
				}
			}
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00006A28 File Offset: 0x00004C28
		public static void ShowEula()
		{
			if (WindowManager._eulaWindow == null)
			{
				WindowManager._eulaWindow = new EulaWindow();
			}
			WindowManager._eulaWindow.Show();
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00006A45 File Offset: 0x00004C45
		public static void NotifyEulaClosed()
		{
			WindowManager._eulaWindow = null;
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00006A4D File Offset: 0x00004C4D
		public static void ShowLoadingSplash()
		{
			Log.Write("Showing loading screen window.");
			if (WindowManager._loadingSplash == null)
			{
				WindowManager._loadingSplash = new LoadingSplash();
			}
			WindowManager._loadingSplash.Show();
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00006A74 File Offset: 0x00004C74
		public static void ShowSelfAutoUpdate()
		{
			Log.Write("Showing self automatic update window.");
			if (WindowManager._selfAutoUpdateWindow == null)
			{
				WindowManager._selfAutoUpdateWindow = new SelfAutoUpdateWindow();
			}
			WindowManager._selfAutoUpdateWindow.Show();
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00006A9B File Offset: 0x00004C9B
		public static void NotifySelfAutoUpdateClosed()
		{
			WindowManager._selfAutoUpdateWindow = null;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00006AA3 File Offset: 0x00004CA3
		public static void ShowMainWindow()
		{
			Log.Write("Showing main window.");
			if (WindowManager._mainWindow == null)
			{
				WindowManager._mainWindow = new MainWindow();
			}
			WindowManager._mainWindow.Show();
			WindowManager._mainWindow.CenterWindowOnScreen();
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00006AD4 File Offset: 0x00004CD4
		public static void NotifyMainWindowClosed()
		{
			WindowManager._mainWindow = null;
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00006ADC File Offset: 0x00004CDC
		public static void AdvancePastEula()
		{
			WindowManager.EndCurrentWindow();
			WindowManager.ShowLoadingSplash();
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00006AE8 File Offset: 0x00004CE8
		public static void AdvanceToMain()
		{
			WindowManager.EndCurrentWindow();
			WindowManager.ShowMainWindow();
		}

		// Token: 0x040000A2 RID: 162
		private static EulaWindow _eulaWindow;

		// Token: 0x040000A3 RID: 163
		private static LoadingSplash _loadingSplash;

		// Token: 0x040000A4 RID: 164
		private static SelfAutoUpdateWindow _selfAutoUpdateWindow;

		// Token: 0x040000A5 RID: 165
		private static MainWindow _mainWindow = new MainWindow();
	}
}
