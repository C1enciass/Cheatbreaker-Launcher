﻿using System;
using System.Collections.Generic;

namespace ClientLauncher
{
	// Token: 0x02000007 RID: 7
	internal class AssetIndex
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600000F RID: 15 RVA: 0x000024E8 File Offset: 0x000006E8
		// (set) Token: 0x06000010 RID: 16 RVA: 0x000024F0 File Offset: 0x000006F0
		public List<IndexObject> _objects
		{
			get;
			set;
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000011 RID: 17 RVA: 0x000024F9 File Offset: 0x000006F9
		public IEnumerable<IndexObject> IndexObject
		{
			get
			{
				return this._objects;
			}
		}
	}
}
