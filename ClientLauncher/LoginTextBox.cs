﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ClientLauncher
{
	// Token: 0x02000030 RID: 48
	public class LoginTextBox : TextBox
	{
		// Token: 0x06000194 RID: 404 RVA: 0x00007000 File Offset: 0x00005200
		static LoginTextBox()
		{
			FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(LoginTextBox), new FrameworkPropertyMetadata(typeof(LoginTextBox)));
		}

		// Token: 0x06000196 RID: 406 RVA: 0x000070B5 File Offset: 0x000052B5
		protected override void OnTextChanged(TextChangedEventArgs e)
		{
			base.OnTextChanged(e);
			this.HasText = (base.Text.Length != 0);
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000197 RID: 407 RVA: 0x000070D2 File Offset: 0x000052D2
		// (set) Token: 0x06000198 RID: 408 RVA: 0x000070E4 File Offset: 0x000052E4
		public string LabelText
		{
			get
			{
				return (string)base.GetValue(LoginTextBox.LabelTextProperty);
			}
			set
			{
				base.SetValue(LoginTextBox.LabelTextProperty, value);
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000199 RID: 409 RVA: 0x000070F2 File Offset: 0x000052F2
		// (set) Token: 0x0600019A RID: 410 RVA: 0x00007104 File Offset: 0x00005304
		public Brush LabelTextColor
		{
			get
			{
				return (Brush)base.GetValue(LoginTextBox.LabelTextColorProperty);
			}
			set
			{
				base.SetValue(LoginTextBox.LabelTextColorProperty, value);
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00007112 File Offset: 0x00005312
		// (set) Token: 0x0600019C RID: 412 RVA: 0x00007124 File Offset: 0x00005324
		public bool HasText
		{
			get
			{
				return (bool)base.GetValue(LoginTextBox.HasTextProperty);
			}
			private set
			{
				base.SetValue(LoginTextBox.HasTextPropertyKey, value);
			}
		}

		// Token: 0x040000B7 RID: 183
		public static DependencyProperty LabelTextProperty = DependencyProperty.Register("LabelText", typeof(string), typeof(LoginTextBox));

		// Token: 0x040000B8 RID: 184
		public static DependencyProperty LabelTextColorProperty = DependencyProperty.Register("LabelTextColor", typeof(Brush), typeof(LoginTextBox));

		// Token: 0x040000B9 RID: 185
		private static DependencyPropertyKey HasTextPropertyKey = DependencyProperty.RegisterReadOnly("HasText", typeof(bool), typeof(LoginTextBox), new PropertyMetadata());

		// Token: 0x040000BA RID: 186
		public static DependencyProperty HasTextProperty = LoginTextBox.HasTextPropertyKey.DependencyProperty;
	}
}
