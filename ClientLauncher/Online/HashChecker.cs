﻿using System;

namespace ClientLauncher.Online
{
	// Token: 0x02000047 RID: 71
	internal class HashChecker
	{
		// Token: 0x060001EE RID: 494 RVA: 0x0000828F File Offset: 0x0000648F
		public HashChecker(string fileEndpoint, string localFilepath)
		{
			this.endpoint = fileEndpoint;
			this.filePath = localFilepath;
		}

		// Token: 0x060001EF RID: 495 RVA: 0x000082A5 File Offset: 0x000064A5
		public bool IsCurrent()
		{
			return false;
		}

		// Token: 0x040000F8 RID: 248
		private string endpoint;

		// Token: 0x040000F9 RID: 249
		private string filePath;
	}
}
