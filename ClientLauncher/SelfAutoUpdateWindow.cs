﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;

namespace ClientLauncher
{
	// Token: 0x0200001D RID: 29
	public class SelfAutoUpdateWindow : Window, IComponentConnector
	{
		// Token: 0x0600010A RID: 266 RVA: 0x0000561B File Offset: 0x0000381B
		public SelfAutoUpdateWindow()
		{
			this.InitializeComponent();
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00003388 File Offset: 0x00001588
		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				base.DragMove();
			}
		}

		// Token: 0x0600010C RID: 268 RVA: 0x0000562C File Offset: 0x0000382C
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/ClientLauncher;component/windows/selfautoupdate/selfautoupdate.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}

		// Token: 0x0600010D RID: 269 RVA: 0x0000565C File Offset: 0x0000385C
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((SelfAutoUpdateWindow)target).MouseDown += new MouseButtonEventHandler(this.Window_MouseDown);
				return;
			}
			this._contentLoaded = true;
		}

		// Token: 0x04000075 RID: 117
		private bool _contentLoaded;
	}
}
