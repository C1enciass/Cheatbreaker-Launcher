﻿using System;
using Newtonsoft.Json;

namespace ClientLauncher.Library
{
	// Token: 0x0200004B RID: 75
	public class NativeArtifact
	{
		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000207 RID: 519 RVA: 0x000084DA File Offset: 0x000066DA
		// (set) Token: 0x06000208 RID: 520 RVA: 0x000084E2 File Offset: 0x000066E2
		[JsonProperty(PropertyName = "natives-windows")]
		public LibraryArtifact artifactUniversal
		{
			get;
			set;
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000209 RID: 521 RVA: 0x000084EB File Offset: 0x000066EB
		// (set) Token: 0x0600020A RID: 522 RVA: 0x000084F3 File Offset: 0x000066F3
		[JsonProperty(PropertyName = "natives-windows-64")]
		public LibraryArtifact artifact64
		{
			get;
			set;
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600020B RID: 523 RVA: 0x000084FC File Offset: 0x000066FC
		// (set) Token: 0x0600020C RID: 524 RVA: 0x00008504 File Offset: 0x00006704
		[JsonProperty(PropertyName = "natives-windows-32")]
		public LibraryArtifact artifact32
		{
			get;
			set;
		}
	}
}
