﻿using System;

namespace ClientLauncher.Library
{
	// Token: 0x02000049 RID: 73
	public class LibraryDownloads
	{
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x00008474 File Offset: 0x00006674
		// (set) Token: 0x060001FA RID: 506 RVA: 0x0000847C File Offset: 0x0000667C
		public LibraryArtifact artifact
		{
			get;
			set;
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001FB RID: 507 RVA: 0x00008485 File Offset: 0x00006685
		// (set) Token: 0x060001FC RID: 508 RVA: 0x0000848D File Offset: 0x0000668D
		public NativeArtifact classifiers
		{
			get;
			set;
		}
	}
}
