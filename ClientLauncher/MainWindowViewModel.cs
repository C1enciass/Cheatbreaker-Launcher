﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Management;
using System.Net.Cache;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ClientLauncher.MojangAuth;

namespace ClientLauncher
{
	// Token: 0x0200001B RID: 27
	public class MainWindowViewModel : INotifyPropertyChanged, ILauncherCallback
	{
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x000044E5 File Offset: 0x000026E5
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x000044ED File Offset: 0x000026ED
		public MainWindow Window
		{
			get;
			set;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x000044F6 File Offset: 0x000026F6
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x000044FE File Offset: 0x000026FE
		public int OvertakePageIndex
		{
			get
			{
				return this._overtakePageIndex;
			}
			set
			{
				if (this._overtakePageIndex != value)
				{
					this._overtakePageIndex = value;
					this.NotifyPropertyChanged("OvertakePageIndex");
				}
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x0000451B File Offset: 0x0000271B
		public string CPUNameText
		{
			get
			{
				return this._cpuNameText;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000A7 RID: 167 RVA: 0x00004523 File Offset: 0x00002723
		public string CPUArchitectureText
		{
			get
			{
				return this._cpuArchitectureText;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000A8 RID: 168 RVA: 0x0000452B File Offset: 0x0000272B
		// (set) Token: 0x060000A9 RID: 169 RVA: 0x00004533 File Offset: 0x00002733
		public int MinGameRAMSetting
		{
			get
			{
				return this._minRAM;
			}
			set
			{
				if (this._minRAM != value)
				{
					this._minRAM = value;
					this.NotifyPropertyChanged("MinGameRAMSetting");
				}
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000AA RID: 170 RVA: 0x00004550 File Offset: 0x00002750
		// (set) Token: 0x060000AB RID: 171 RVA: 0x00004558 File Offset: 0x00002758
		public int MaxGameRAMSetting
		{
			get
			{
				return this._maxRAM;
			}
			set
			{
				if (this._maxRAM != value)
				{
					this._maxRAM = value;
					this.NotifyPropertyChanged("MaxGameRAMSetting");
				}
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000AC RID: 172 RVA: 0x00004575 File Offset: 0x00002775
		// (set) Token: 0x060000AD RID: 173 RVA: 0x0000457D File Offset: 0x0000277D
		public int ActiveTabIndex
		{
			get
			{
				return this._activeTabIndex;
			}
			set
			{
				if (this._activeTabIndex != value)
				{
					this._activeTabIndex = value;
					if (4 != value)
					{
						this._nonLaunchTabIndex = value;
					}
					this.NotifyPropertyChanged("ActiveTabIndex");
				}
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000AE RID: 174 RVA: 0x000045A5 File Offset: 0x000027A5
		// (set) Token: 0x060000AF RID: 175 RVA: 0x000045AD File Offset: 0x000027AD
		public ObservableCollection<MinecraftAccount> ExistingUsers
		{
			get
			{
				return this._existingUsers;
			}
			set
			{
				if (this._existingUsers != value)
				{
					this._existingUsers = value;
					this.NotifyPropertyChanged("ExistingUsers");
				}
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x000045CA File Offset: 0x000027CA
		// (set) Token: 0x060000B1 RID: 177 RVA: 0x000045D2 File Offset: 0x000027D2
		public MinecraftAccount SelectedUser
		{
			get
			{
				return this._selectedUser;
			}
			set
			{
				if (this._selectedUser != value)
				{
					this.InternalSetSelectedUser(value);
					this.ValidateSelectedUser();
				}
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x000045EA File Offset: 0x000027EA
		private void InternalSetSelectedUser(MinecraftAccount account)
		{
			if (this._selectedUser != account)
			{
				this._selectedUser = account;
				this.NotifyPropertyChanged("SelectedUser");
				this.NotifyPropertyChanged("SelectedUserDisplayIcon");
				this.NotifyPropertyChanged("SelectedUserDisplayName");
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x00004620 File Offset: 0x00002820
		public ImageSource SelectedUserDisplayIcon
		{
			get
			{
				if (this._selectedUser == null)
				{
					return null;
				}
				string text = "https://visage.surgeplay.com/face/" + this._selectedUser.UUIDTrimmed;
				if (this._selectedUserImageUrl != text)
				{
					this._selectedUserImageUrl = text;
					this._selectedUserImage = new BitmapImage(new Uri(this._selectedUserImageUrl), new RequestCachePolicy(RequestCacheLevel.CacheIfAvailable));
				}
				return this._selectedUserImage;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x00004684 File Offset: 0x00002884
		public string SelectedUserDisplayName
		{
			get
			{
				if (this._selectedUser == null)
				{
					return "Click to login";
				}
				return this._selectedUser.DisplayName;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x0000469F File Offset: 0x0000289F
		// (set) Token: 0x060000B6 RID: 182 RVA: 0x000046A7 File Offset: 0x000028A7
		public string LaunchStatusText
		{
			get
			{
				return this._launchStatusText;
			}
			set
			{
				this._launchStatusText = value;
				this.NotifyPropertyChanged("LaunchStatusText");
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x000046BB File Offset: 0x000028BB
		// (set) Token: 0x060000B8 RID: 184 RVA: 0x000046C3 File Offset: 0x000028C3
		public int LaunchProgress
		{
			get
			{
				return this._launchProgress;
			}
			set
			{
				this._launchProgress = value;
				this.NotifyPropertyChanged("LaunchProgress");
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x000046D7 File Offset: 0x000028D7
		// (set) Token: 0x060000BA RID: 186 RVA: 0x000046DF File Offset: 0x000028DF
		public ObservableCollection<NewsEntry> NewsData
		{
			get
			{
				return this._newsData;
			}
			set
			{
				if (this._newsData != value)
				{
					this._newsData = value;
					this.NotifyPropertyChanged("NewsData");
				}
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000BB RID: 187 RVA: 0x000046FC File Offset: 0x000028FC
		// (set) Token: 0x060000BC RID: 188 RVA: 0x00004704 File Offset: 0x00002904
		public string CurrentPassword
		{
			get
			{
				return this._currentPassword;
			}
			set
			{
				this._currentPassword = value;
				this.NotifyPropertyChanged("CurrentPassword");
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000BD RID: 189 RVA: 0x00004718 File Offset: 0x00002918
		// (set) Token: 0x060000BE RID: 190 RVA: 0x00004720 File Offset: 0x00002920
		public string LoginErrorText
		{
			get
			{
				return this._loginErrorText;
			}
			set
			{
				this._loginErrorText = value;
				this.NotifyPropertyChanged("LoginErrorText");
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00004734 File Offset: 0x00002934
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x0000473C File Offset: 0x0000293C
		public RelayCommand LaunchCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x00004745 File Offset: 0x00002945
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x0000474D File Offset: 0x0000294D
		public ICommand MinimizeCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00004756 File Offset: 0x00002956
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x0000475E File Offset: 0x0000295E
		public ICommand CloseCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00004767 File Offset: 0x00002967
		// (set) Token: 0x060000C6 RID: 198 RVA: 0x0000476F File Offset: 0x0000296F
		public ICommand ShowHomeCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x00004778 File Offset: 0x00002978
		// (set) Token: 0x060000C8 RID: 200 RVA: 0x00004780 File Offset: 0x00002980
		public ICommand ShowSettingsCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00004789 File Offset: 0x00002989
		// (set) Token: 0x060000CA RID: 202 RVA: 0x00004791 File Offset: 0x00002991
		public ICommand ShowServersCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000CB RID: 203 RVA: 0x0000479A File Offset: 0x0000299A
		// (set) Token: 0x060000CC RID: 204 RVA: 0x000047A2 File Offset: 0x000029A2
		public ICommand ShowAboutCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000CD RID: 205 RVA: 0x000047AB File Offset: 0x000029AB
		// (set) Token: 0x060000CE RID: 206 RVA: 0x000047B3 File Offset: 0x000029B3
		public ICommand ShowAccountsCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000CF RID: 207 RVA: 0x000047BC File Offset: 0x000029BC
		// (set) Token: 0x060000D0 RID: 208 RVA: 0x000047C4 File Offset: 0x000029C4
		public ICommand ShowSignInCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x000047CD File Offset: 0x000029CD
		// (set) Token: 0x060000D2 RID: 210 RVA: 0x000047D5 File Offset: 0x000029D5
		public ICommand ShowLaunchingCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x000047DE File Offset: 0x000029DE
		// (set) Token: 0x060000D4 RID: 212 RVA: 0x000047E6 File Offset: 0x000029E6
		public ICommand SelectOnLaunchOptionCommand
		{
			get;
			internal set;
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x000047EF File Offset: 0x000029EF
		// (set) Token: 0x060000D6 RID: 214 RVA: 0x000047F7 File Offset: 0x000029F7
		public ICommand LoginCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x00004800 File Offset: 0x00002A00
		// (set) Token: 0x060000D8 RID: 216 RVA: 0x00004808 File Offset: 0x00002A08
		public ICommand SelectAccountCommand
		{
			get;
			internal set;
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00004814 File Offset: 0x00002A14
		public MainWindowViewModel()
		{
			this.LaunchCommand = new RelayCommand(new Action<object>(this.LaunchCommandExecuted), new Predicate<object>(this.CanLaunchCommandExecute));
			this.MinimizeCommand = new RelayCommand(new Action<object>(this.MinimizeCommandExecuted));
			this.CloseCommand = new RelayCommand(new Action<object>(this.CloseCommandExecuted));
			this.ShowHomeCommand = new RelayCommand(new Action<object>(this.ShowHomeCommandExecuted));
			this.ShowServersCommand = new RelayCommand(new Action<object>(this.ShowServersCommandExecuted));
			this.ShowSettingsCommand = new RelayCommand(new Action<object>(this.ShowSettingsCommandExecuted));
			this.ShowAboutCommand = new RelayCommand(new Action<object>(this.ShowAboutCommandExecuted));
			this.ShowAccountsCommand = new RelayCommand(new Action<object>(this.ShowAccountsCommandExecuted));
			this.ShowSignInCommand = new RelayCommand(new Action<object>(this.ShowSignInCommandExecuted));
			this.ShowLaunchingCommand = new RelayCommand(new Action<object>(this.ShowLaunchingCommandExecuted));
			this.SelectOnLaunchOptionCommand = new RelayCommand(new Action<object>(this.SelectOnLaunchOptionCommandExecuted));
			this.LoginCommand = new RelayCommand(new Action<object>(this.LoginCommandExecuted), new Predicate<object>(this.CanLoginCommandExecute));
			this.SelectAccountCommand = new RelayCommand(new Action<object>(this.SelectAccountCommandExecuted));
			try
			{
				using (ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Processor"))
				{
					using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							ManagementObject managementObject = (ManagementObject)enumerator.Current;
							using (managementObject)
							{
								this._cpuNameText = managementObject["Name"].ToString().Trim();
								break;
							}
						}
					}
				}
			}
			catch
			{
			}
			this._cpuArchitectureText = (Environment.Is64BitOperatingSystem ? "x64" : "x86");
			try
			{
				this._maxRAM = 1048576;
				using (ManagementObjectSearcher managementObjectSearcher2 = new ManagementObjectSearcher(new ObjectQuery("SELECT * FROM Win32_OperatingSystem")))
				{
					using (ManagementObjectCollection managementObjectCollection = managementObjectSearcher2.Get())
					{
						using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectCollection.GetEnumerator())
						{
							while (enumerator.MoveNext())
							{
								ManagementObject managementObject3 = (ManagementObject)enumerator.Current;
								using (managementObject3)
								{
									this._maxRAM = (int)(Convert.ToDouble(managementObject3["TotalVisibleMemorySize"]) / 1024.0);
								}
							}
						}
					}
				}
			}
			catch
			{
			}
			this._checkGameRunningTimer = new Timer(new TimerCallback(this.CheckGameRunning), null, 0, 1000);
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00004BC4 File Offset: 0x00002DC4
		public void Initialize()
		{
			this.LoadAccounts();
			this.RefreshNews(false);
			this.ShowMainPageOrAccountSelector();
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00004BDC File Offset: 0x00002DDC
		public void SaveProfiles()
		{
			List<MinecraftAccount> accounts = new List<MinecraftAccount>(this.ExistingUsers);
			MojangData expr_11 = MojangData.GetSingleton();
			expr_11.LauncherProfiles.SetAccounts(accounts);
			expr_11.LauncherProfiles.Save();
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00004C10 File Offset: 0x00002E10
		public bool LoadAccounts()
		{
			List<MinecraftAccount> arg_3E_0 = MojangData.GetSingleton().LauncherProfiles.GetAccounts();
			Authenticator.ClientToken = MojangData.GetSingleton().LauncherProfiles.GetClientToken();
			this.ExistingUsers.Clear();
			string selectedUserUUID = MojangData.GetSingleton().LauncherProfiles.GetSelectedUserUUID();
			foreach (MinecraftAccount current in arg_3E_0)
			{
				this.ExistingUsers.Add(current);
				if (selectedUserUUID.Equals(current.UUIDTrimmed))
				{
					this.SelectedUser = current;
				}
			}
			this.RefreshExistingUsers();
			this.SaveProfiles();
			return this.SelectedUser != null;
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00004CCC File Offset: 0x00002ECC
		public void RefreshExistingUsers()
		{
			this.NotifyPropertyChanged("ExistingUsers");
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000029D8 File Offset: 0x00000BD8
		private void ShowNotLoggedInError()
		{
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00004CD9 File Offset: 0x00002ED9
		private bool CanLaunchCommandExecute(object param)
		{
			return this.SelectedUser != null && !this._runningGame;
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00004CEE File Offset: 0x00002EEE
		private void LaunchCommandExecuted(object param)
		{
			this.BeginLaunch(param as string);
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00004CFC File Offset: 0x00002EFC
		private void MinimizeCommandExecuted(object param)
		{
			try
			{
				this.Window.WindowState = WindowState.Minimized;
			}
			catch
			{
			}
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00004D2C File Offset: 0x00002F2C
		private void CloseCommandExecuted(object param)
		{
			try
			{
				this.Window.Close();
			}
			catch
			{
			}
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00004D5C File Offset: 0x00002F5C
		private void ShowHomeCommandExecuted(object param)
		{
			this.ActiveTabIndex = 0;
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x00004D65 File Offset: 0x00002F65
		// (set) Token: 0x060000E5 RID: 229 RVA: 0x00004D6D File Offset: 0x00002F6D
		public string MineHQPingText
		{
			get
			{
				return this._minehqPingText;
			}
			set
			{
				if (this._minehqPingText != value)
				{
					this._minehqPingText = value;
					this.NotifyPropertyChanged("MineHQPingText");
				}
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x00004D8F File Offset: 0x00002F8F
		// (set) Token: 0x060000E7 RID: 231 RVA: 0x00004D97 File Offset: 0x00002F97
		public string VeltPingText
		{
			get
			{
				return this._veltPingText;
			}
			set
			{
				if (this._veltPingText != value)
				{
					this._veltPingText = value;
					this.NotifyPropertyChanged("VeltPingText");
				}
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x00004DB9 File Offset: 0x00002FB9
		// (set) Token: 0x060000E9 RID: 233 RVA: 0x00004DC1 File Offset: 0x00002FC1
		public string ArcanePingText
		{
			get
			{
				return this._arcanePingText;
			}
			set
			{
				if (this._arcanePingText != value)
				{
					this._arcanePingText = value;
					this.NotifyPropertyChanged("ArcanePingText");
				}
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00004DE4 File Offset: 0x00002FE4
		private void MinecraftPingCallback(MinecraftPingResult result)
		{
			string host = result.host;
			if (host == "minehq.com")
			{
				this.MineHQPingText = (result.success ? string.Format("{0} ONLINE", result.numPlayers) : string.Empty);
				return;
			}
			if (host == "mc.veltpvp.com")
			{
				this.VeltPingText = (result.success ? string.Format("{0} ONLINE", result.numPlayers) : string.Empty);
				return;
			}
			if (!(host == "arcane.cc"))
			{
				return;
			}
			this.ArcanePingText = (result.success ? string.Format("{0} ONLINE", result.numPlayers) : string.Empty);
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00004EA0 File Offset: 0x000030A0
		private void ShowServersCommandExecuted(object param)
		{
			this.ActiveTabIndex = 1;
			MinecraftServerQuery.PingAsync("minehq.com", 25565, 10000, new Action<MinecraftPingResult>(this.MinecraftPingCallback));
			MinecraftServerQuery.PingAsync("mc.veltpvp.com", 25565, 10000, new Action<MinecraftPingResult>(this.MinecraftPingCallback));
			MinecraftServerQuery.PingAsync("arcane.cc", 25565, 10000, new Action<MinecraftPingResult>(this.MinecraftPingCallback));
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00004F14 File Offset: 0x00003114
		private void ShowSettingsCommandExecuted(object param)
		{
			this.ActiveTabIndex = 2;
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00004F1D File Offset: 0x0000311D
		private void ShowAboutCommandExecuted(object param)
		{
			this.ActiveTabIndex = 3;
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00004F26 File Offset: 0x00003126
		private void ShowAccountsCommandExecuted(object param)
		{
			if (this.ExistingUsers.Count == 0)
			{
				this.OvertakePageIndex = 2;
				return;
			}
			this.OvertakePageIndex = 1;
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00004F44 File Offset: 0x00003144
		private void ShowSignInCommandExecuted(object param)
		{
			this.OvertakePageIndex = 2;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00004F4D File Offset: 0x0000314D
		private void ShowLaunchingCommandExecuted(object param)
		{
			this.ActiveTabIndex = 4;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00004F56 File Offset: 0x00003156
		private void SelectOnLaunchOptionCommandExecuted(object param)
		{
			CheatBreakerSettings.GetInstance().LaunchBehavior = Convert.ToInt32(param);
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004F68 File Offset: 0x00003168
		public void BeginLaunch(string serverAddress)
		{
			if (this.SelectedUser == null)
			{
				this.ShowNotLoggedInError();
				return;
			}
			if (!this._launching)
			{
				Log.Write("Validating selected user before launch...");
				this.ValidateSelectedUser();
				if (this.SelectedUser != null)
				{
					Log.Write("Initializing launch...");
					Launcher.GetSingleton().Launch(this, this.SelectedUser, serverAddress);
				}
			}
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00004FC0 File Offset: 0x000031C0
		public void UpdateLauncherText(string statusText)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(delegate
			{
				this.LaunchStatusText = statusText;
			}), new object[0]);
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00005004 File Offset: 0x00003204
		public void UpdateLauncherProgress(int progress)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(delegate
			{
				this.LaunchProgress = progress;
			}), new object[0]);
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00005047 File Offset: 0x00003247
		public void UpdateLaunching(bool launching)
		{
			if (launching != this._launching)
			{
				this._launching = launching;
				Application.Current.Dispatcher.BeginInvoke(new Action(delegate
				{
					if (this._launching)
					{
						this.ActiveTabIndex = 4;
						return;
					}
					this.ActiveTabIndex = this._nonLaunchTabIndex;
				}), new object[0]);
			}
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x0000507B File Offset: 0x0000327B
		public void UpdateGameRunning(bool running)
		{
			if (running != this._runningGame)
			{
				this._runningGame = running;
				Application.Current.Dispatcher.BeginInvoke(new Action(delegate
				{
					this.LaunchCommand.OnCanExecuteChanged();
					if (this._runningGame)
					{
						try
						{
							switch (CheatBreakerSettings.GetInstance().LaunchBehavior)
							{
							case 0:
								this.Window.WindowState = WindowState.Minimized;
								break;
							}
							return;
						}
						catch
						{
							return;
						}
					}
					switch (CheatBreakerSettings.GetInstance().LaunchBehavior)
					{
					case 0:
						this.Window.WindowState = WindowState.Normal;
						break;
					}
					this.RefreshNews(true);
				}), new object[0]);
			}
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x000050AF File Offset: 0x000032AF
		public void RefreshNews(bool globalRefresh)
		{
			if (globalRefresh)
			{
				CheatBreakerGlobals.GetSingleton().FetchNews();
			}
			this.NewsData = CheatBreakerGlobals.GetSingleton().NewsData;
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x000050CE File Offset: 0x000032CE
		private bool CanLoginCommandExecute(object param)
		{
			return this.Window != null && !string.IsNullOrEmpty(this.Window.LoginEmailTextBox.Text) && !string.IsNullOrEmpty(this.Window.LoginPasswordTextBox.Password);
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x0000510C File Offset: 0x0000330C
		private void LoginCommandExecuted(object param)
		{
			string text = this.Window.LoginEmailTextBox.Text;
			string password = this.Window.LoginPasswordTextBox.Password;
			int num = 0;
			LoginResponse response = (LoginResponse)Authenticator.GetResponse<LoginResponse>(new AuthPayload(text, password, Authenticator.ClientToken, new LoginAgent()), "authenticate", out num);
			if (response != null && response.errorMessage == null && response.IsValidAccount())
			{
				Authenticator.ClientToken = response.clientToken;
				MinecraftAccount minecraftAccount = new MinecraftAccount(response.selectedProfile.name, response.accessToken, response.user.id, response.selectedProfile.id, text);
				(from other in this.ExistingUsers
				where other.UserID == response.user.id
				select other).ToList<MinecraftAccount>().ForEach(delegate(MinecraftAccount entry)
				{
					this.ExistingUsers.Remove(entry);
				});
				this.ExistingUsers.Add(minecraftAccount);
				this.SelectedUser = minecraftAccount;
				this.SaveProfiles();
				this.RefreshExistingUsers();
				this.Window.AccountListBox.Visibility = ((this.ExistingUsers.Count != 0) ? Visibility.Visible : Visibility.Collapsed);
				this.ShowAccountsCommand.Execute(null);
			}
			else if (response != null && response.errorMessage != null)
			{
				this.LoginErrorText = "Login error. Server said: " + Environment.NewLine + response.errorMessage;
			}
			else
			{
				this.LoginErrorText = "There was a failure attempting to login.";
			}
			this.Window.LoginPasswordTextBox.Password = "";
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000052BD File Offset: 0x000034BD
		private void ShowMainPageOrAccountSelector()
		{
			if (this.SelectedUser != null)
			{
				this.OvertakePageIndex = 0;
				return;
			}
			if (this.ExistingUsers.Count == 0)
			{
				this.OvertakePageIndex = 2;
				return;
			}
			this.OvertakePageIndex = 1;
		}

		// Token: 0x060000FB RID: 251 RVA: 0x000052EC File Offset: 0x000034EC
		private void SelectAccountCommandExecuted(object param)
		{
			MinecraftAccount minecraftAccount = param as MinecraftAccount;
			if (minecraftAccount != null)
			{
				this.SelectedUser = minecraftAccount;
				this.ShowMainPageOrAccountSelector();
			}
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00005310 File Offset: 0x00003510
		private void CheckGameRunning(object state)
		{
			if (Launcher.IsCheatBreakerClientAlreadyRunning())
			{
				this.UpdateGameRunning(true);
				return;
			}
			this.UpdateGameRunning(false);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00005328 File Offset: 0x00003528
		private void ValidateSelectedUser()
		{
			MojangData singleton = MojangData.GetSingleton();
			if (this._selectedUser == null)
			{
				singleton.LauncherProfiles.SetSelectedUser(this._selectedUser);
				this.SaveProfiles();
			}
			else if (singleton.LauncherProfiles.GetSelectedUserUUID() != this._selectedUser.UUIDTrimmed)
			{
				if (singleton.LauncherProfiles.ValidateAccount(this._selectedUser))
				{
					Log.Write(string.Format("Selecting account {0} with access token {1}.", this._selectedUser.DetailedDisplayName, this._selectedUser.AccessToken));
					singleton.LauncherProfiles.SetSelectedUser(this._selectedUser);
				}
				else
				{
					Log.Write(string.Format("Not selecting, instead removing account {0} with access token {1}. Not valid.", this._selectedUser.DetailedDisplayName, this._selectedUser.AccessToken));
					if (this.ExistingUsers.Contains(this._selectedUser))
					{
						this.ExistingUsers.Remove(this._selectedUser);
					}
					singleton.LauncherProfiles.SetSelectedUser(null);
					this.InternalSetSelectedUser(null);
				}
				this.SaveProfiles();
			}
			this.ShowMainPageOrAccountSelector();
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060000FE RID: 254 RVA: 0x00005434 File Offset: 0x00003634
		// (remove) Token: 0x060000FF RID: 255 RVA: 0x0000546C File Offset: 0x0000366C
		[method: CompilerGenerated]
		[CompilerGenerated]
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x06000100 RID: 256 RVA: 0x000054A1 File Offset: 0x000036A1
		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x0400004D RID: 77
		private int _minRAM = 1024;

		// Token: 0x0400004E RID: 78
		private int _maxRAM = 1024;

		// Token: 0x0400004F RID: 79
		private int _nonLaunchTabIndex;

		// Token: 0x04000050 RID: 80
		private int _activeTabIndex;

		// Token: 0x04000051 RID: 81
		private string _launchStatusText = "-";

		// Token: 0x04000052 RID: 82
		private int _launchProgress = 50;

		// Token: 0x04000053 RID: 83
		private bool _launching;

		// Token: 0x04000054 RID: 84
		private bool _runningGame;

		// Token: 0x04000055 RID: 85
		private string _cpuNameText = "Unknown";

		// Token: 0x04000056 RID: 86
		private string _cpuArchitectureText = string.Empty;

		// Token: 0x04000057 RID: 87
		private string _loginErrorText = string.Empty;

		// Token: 0x04000058 RID: 88
		private ObservableCollection<NewsEntry> _newsData = new ObservableCollection<NewsEntry>();

		// Token: 0x04000059 RID: 89
		private int _overtakePageIndex;

		// Token: 0x0400005A RID: 90
		private string _currentPassword = string.Empty;

		// Token: 0x0400005B RID: 91
		private Timer _checkGameRunningTimer;

		// Token: 0x0400005D RID: 93
		private ObservableCollection<MinecraftAccount> _existingUsers = new ObservableCollection<MinecraftAccount>();

		// Token: 0x0400005E RID: 94
		private MinecraftAccount _selectedUser;

		// Token: 0x0400005F RID: 95
		private BitmapImage _selectedUserImage;

		// Token: 0x04000060 RID: 96
		private string _selectedUserImageUrl = string.Empty;

		// Token: 0x0400006E RID: 110
		private string _minehqPingText = string.Empty;

		// Token: 0x0400006F RID: 111
		private string _veltPingText = string.Empty;

		// Token: 0x04000070 RID: 112
		private string _arcanePingText = string.Empty;
	}
}
