﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace ClientLauncher
{
	// Token: 0x02000033 RID: 51
	public class MainWindow : Window, IComponentConnector
	{
		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x0000745F File Offset: 0x0000565F
		public MainWindowViewModel ViewModel
		{
			get
			{
				return this._viewModel;
			}
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00007467 File Offset: 0x00005667
		public MainWindow()
		{
			this._viewModel = new MainWindowViewModel();
			this._viewModel.Window = this;
			base.DataContext = this._viewModel;
			this.InitializeComponent();
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00003388 File Offset: 0x00001588
		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
			{
				base.DragMove();
			}
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00007498 File Offset: 0x00005698
		private void User_Click(object sender, RoutedEventArgs e)
		{
			this._viewModel.ShowAccountsCommand.Execute(null);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x000074AB File Offset: 0x000056AB
		private void Window_Closed(object sender, EventArgs e)
		{
			App.Singleton.Quit();
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x000074B8 File Offset: 0x000056B8
		public void CenterWindowOnScreen()
		{
			double primaryScreenWidth = SystemParameters.PrimaryScreenWidth;
			double primaryScreenHeight = SystemParameters.PrimaryScreenHeight;
			double num = 1152.0;
			double num2 = 648.0;
			base.Left = primaryScreenWidth / 2.0 - num / 2.0;
			base.Top = primaryScreenHeight / 2.0 - num2 / 2.0;
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00007520 File Offset: 0x00005720
		private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
		{
			try
			{
				Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
				e.Handled = true;
			}
			catch
			{
			}
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00007560 File Offset: 0x00005760
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			CheatBreakerGlobals.GetSingleton().Window = this;
			this._viewModel.Initialize();
			this.AccountListBox.Visibility = ((this._viewModel.ExistingUsers.Count != 0) ? Visibility.Visible : Visibility.Collapsed);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x000029D8 File Offset: 0x00000BD8
		private void Window_StateChanged(object sender, EventArgs e)
		{
		}

		// Token: 0x060001BA RID: 442 RVA: 0x000029D8 File Offset: 0x00000BD8
		private void Window_Activated(object sender, EventArgs e)
		{
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000029D8 File Offset: 0x00000BD8
		private void Window_Deactivated(object sender, EventArgs e)
		{
		}

		// Token: 0x060001BC RID: 444 RVA: 0x0000759C File Offset: 0x0000579C
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (this._contentLoaded)
			{
				return;
			}
			this._contentLoaded = true;
			Uri resourceLocator = new Uri("/ClientLauncher;component/windows/main/mainwindow.xaml", UriKind.Relative);
			Application.LoadComponent(this, resourceLocator);
		}

		// Token: 0x060001BD RID: 445 RVA: 0x000075CC File Offset: 0x000057CC
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		// Token: 0x060001BE RID: 446 RVA: 0x000075D8 File Offset: 0x000057D8
		[DebuggerNonUserCode, GeneratedCode("PresentationBuildTasks", "4.0.0.0"), EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((MainWindow)target).Loaded += new RoutedEventHandler(this.Window_Loaded);
				((MainWindow)target).MouseDown += new MouseButtonEventHandler(this.Window_MouseDown);
				((MainWindow)target).Closed += new EventHandler(this.Window_Closed);
				((MainWindow)target).StateChanged += new EventHandler(this.Window_StateChanged);
				((MainWindow)target).Activated += new EventHandler(this.Window_Activated);
				((MainWindow)target).Deactivated += new EventHandler(this.Window_Deactivated);
				return;
			case 2:
				this.AccountListBox = (ListBox)target;
				return;
			case 3:
				this.LoginEmailTextBox = (LoginTextBox)target;
				return;
			case 4:
				this.LoginPasswordTextBox = (PasswordBox)target;
				return;
			default:
				this._contentLoaded = true;
				return;
			}
		}

		// Token: 0x040000C2 RID: 194
		private const int WINDOW_WIDTH = 1152;

		// Token: 0x040000C3 RID: 195
		private const int WINDOW_HEIGHT = 648;

		// Token: 0x040000C4 RID: 196
		private MainWindowViewModel _viewModel;

		// Token: 0x040000C5 RID: 197
		internal ListBox AccountListBox;

		// Token: 0x040000C6 RID: 198
		internal LoginTextBox LoginEmailTextBox;

		// Token: 0x040000C7 RID: 199
		internal PasswordBox LoginPasswordTextBox;

		// Token: 0x040000C8 RID: 200
		private bool _contentLoaded;
	}
}
