﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using ClientLauncher.Library;
using Newtonsoft.Json.Linq;

namespace ClientLauncher
{
	// Token: 0x0200000F RID: 15
	public class GameFilesUpdater
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00003094 File Offset: 0x00001294
		public List<JsonLibrary> Libraries
		{
			get
			{
				return this._libraries;
			}
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000309C File Offset: 0x0000129C
		private void DownloadFile(string urlSource, string targetFile)
		{
			using (WebClient webClient = new WebClient())
			{
				try
				{
					Log.Write(string.Format("Downloading file from {0}", urlSource));
					webClient.DownloadFile(new Uri(urlSource), targetFile);
					Log.Write("Download complete.");
				}
				catch (Exception expr_2F)
				{
					Log.WriteException(expr_2F, string.Format("Failed to download file information from {0} for {1}", urlSource, targetFile));
					throw expr_2F;
				}
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00003114 File Offset: 0x00001314
		private void DownloadDependenciesJson()
		{
			string text = CheatBreakerGlobals.GetVersionDir("1.7.10") + "1.7.10.json";
			this.DownloadFile("https://files.cheatbreaker.com/1.7.10.json", text);
			try
			{
				string json = File.ReadAllText(text);
				this._dependenciesJson = JObject.Parse(json);
			}
			catch (Exception expr_36)
			{
				Log.WriteException(expr_36, "Failed to parse dependencies information.");
				throw expr_36;
			}
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00003174 File Offset: 0x00001374
		private void DownloadAssetsJson()
		{
			string text;
			try
			{
				string str = (string)this._dependenciesJson.SelectToken("assetIndex").SelectToken("id");
				(string)this._dependenciesJson.SelectToken("assetIndex").SelectToken("url");
				text = CheatBreakerGlobals.GetAssetsDir() + "indexes\\" + str + ".json";
			}
			catch (Exception expr_58)
			{
				Log.WriteException(expr_58, "Failed to parse assetIndex token.");
				throw expr_58;
			}
			this.DownloadFile((string)this._dependenciesJson.SelectToken("assetIndex").SelectToken("url"), text);
			try
			{
				string json = File.ReadAllText(text);
				this._assetsJson = JObject.Parse(json);
			}
			catch (Exception expr_9F)
			{
				Log.WriteException(expr_9F, "Failed to parse assets information.");
				throw expr_9F;
			}
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00003248 File Offset: 0x00001448
		public void DoUpdate(ILauncherCallback callback, Downloader downloader)
		{
			Log.Write("Retrieving update information...");
			callback.UpdateLauncherText("Retrieving update information...");
			this.DownloadDependenciesJson();
			this.DownloadAssetsJson();
			Log.Write("Updating game libraries...");
			callback.UpdateLauncherText("Updating game libraries...");
			LibraryManager libraryManager = new LibraryManager();
			this._libraries = libraryManager.GetForVersion(downloader, "1.7.10", this._dependenciesJson);
			Log.Write("Updating game assets...");
			callback.UpdateLauncherText("Updating game assets...");
			new AssetsManager().GetForVersion(downloader, "1.7.10", this._assetsJson);
		}

		// Token: 0x04000028 RID: 40
		private const string DEPENDENCIES_VERSION = "1.7.10";

		// Token: 0x04000029 RID: 41
		private const string DEPENDENCIES_JSON_URL = "https://files.cheatbreaker.com/1.7.10.json";

		// Token: 0x0400002A RID: 42
		private JObject _dependenciesJson;

		// Token: 0x0400002B RID: 43
		private JObject _assetsJson;

		// Token: 0x0400002C RID: 44
		private List<JsonLibrary> _libraries;
	}
}
