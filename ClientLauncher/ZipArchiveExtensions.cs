﻿using System;
using System.IO;
using System.IO.Compression;

namespace ClientLauncher
{
	// Token: 0x02000023 RID: 35
	public static class ZipArchiveExtensions
	{
		// Token: 0x06000133 RID: 307 RVA: 0x00005CD8 File Offset: 0x00003ED8
		public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
		{
			if (!overwrite)
			{
				archive.ExtractToDirectory(destinationDirectoryName);
				return;
			}
			foreach (ZipArchiveEntry current in archive.Entries)
			{
				string text = Path.Combine(destinationDirectoryName, current.FullName);
				if (current.Name == "")
				{
					try
					{
						Utility.TryCreateDirectory(Path.GetDirectoryName(text));
						continue;
					}
					catch
					{
						continue;
					}
				}
				current.ExtractToFile(text, true);
			}
		}
	}
}
