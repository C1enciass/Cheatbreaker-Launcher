﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ClientLauncher.Cert;
using ClientLauncher.Library;
using ClientLauncher.MojangAuth;

namespace ClientLauncher
{
	// Token: 0x02000014 RID: 20
	public class Launcher
	{
		// Token: 0x0600006A RID: 106 RVA: 0x0000342C File Offset: 0x0000162C
		public static Launcher GetSingleton()
		{
			if (Launcher._instance == null)
			{
				object lockObject = Launcher._lockObject;
				lock (lockObject)
				{
					if (Launcher._instance == null)
					{
						Launcher._instance = new Launcher();
					}
				}
			}
			return Launcher._instance;
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00003484 File Offset: 0x00001684
		private void HandleLaunchError(string error)
		{
			this._lastLaunchError = error;
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00003490 File Offset: 0x00001690
		public static bool IsCheatBreakerClientAlreadyRunning()
		{
			bool result = false;
			try
			{
				Mutex mutex = null;
				if (Mutex.TryOpenExisting("CHEATBREAKERCLIENT", out mutex))
				{
					using (mutex)
					{
						result = true;
					}
				}
			}
			catch (Exception)
			{
			}
			return result;
		}

		// Token: 0x0600006D RID: 109 RVA: 0x000034E8 File Offset: 0x000016E8
		private string GetLaunchArguments(string version, MinecraftAccount selectedAccount, List<JsonLibrary> libraries, string serverAddress)
		{
			string text = "";
			text = text + "\"" + CheatBreakerGlobals.GetJreDir() + "\\bin\\server\\jvm.dll\" ";
			text = text + "\"" + CheatBreakerGlobals.GetClientPatchPath() + "\" ";
			text = text + " -Djava.library.path=\"" + CheatBreakerGlobals.GetNativesDir(version).Replace("\\", "\\\\") + "\"";
			text = string.Concat(new object[]
			{
				text,
				" -Xmx",
				CheatBreakerSettings.GetInstance().RAMSetting,
				"m "
			});
			text += "\"";
			text = text + CheatBreakerGlobals.GetMinecraftJar(version) + ";";
			foreach (JsonLibrary current in libraries)
			{
				text = text + current.GetLocalPath(version) + ";";
			}
			text += "\"";
			text = text + " --username " + selectedAccount.DisplayName;
			text = text + " --uuid " + selectedAccount.UUID;
			text = text + " --accessToken " + selectedAccount.AccessToken;
			text = text + " --version " + version + " ";
			text += " --userProperties {} ";
			text = text + " --gameDir \"" + CheatBreakerGlobals.GetRealMinecraftDir().Replace("\\", "\\\\") + "\" ";
			text = text + " --assetIndex " + version;
			text += " --assetsDir assets";
			if (serverAddress != null)
			{
				text = text + " --server " + serverAddress;
			}
			return text;
		}

		// Token: 0x0600006E RID: 110 RVA: 0x0000369C File Offset: 0x0000189C
		private string GetFileSHA1(string path)
		{
			string result;
			try
			{
				using (SHA1 sHA = SHA1.Create())
				{
					using (FileStream fileStream = File.OpenRead(path))
					{
						result = BitConverter.ToString(sHA.ComputeHash(fileStream)).Replace("-", "").ToLowerInvariant();
					}
				}
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00003720 File Offset: 0x00001920
		private string GetFileMD5(string path)
		{
			string result;
			try
			{
				using (MD5 mD = MD5.Create())
				{
					using (FileStream fileStream = File.OpenRead(path))
					{
						result = BitConverter.ToString(mD.ComputeHash(fileStream)).Replace("-", "").ToLowerInvariant();
					}
				}
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x06000070 RID: 112 RVA: 0x000037A4 File Offset: 0x000019A4
		public void HandleDownloadComplete(object sender, AsyncCompletedEventArgs e)
		{
			object userState = e.UserState;
			lock (userState)
			{
				Monitor.Pulse(e.UserState);
			}
		}

		// Token: 0x06000071 RID: 113 RVA: 0x000037EC File Offset: 0x000019EC
		private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
		{
			this._callback.UpdateLauncherProgress(this._minProgress + (this._maxProgress - this._minProgress) * e.ProgressPercentage / 100);
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003818 File Offset: 0x00001A18
		private void UpdateGameFile(string urlSource, string targetFile, int minProgress, int maxProgress)
		{
			this._minProgress = minProgress;
			this._maxProgress = maxProgress;
			string fileSHA = this.GetFileSHA1(targetFile);
			string text = null;
			using (WebClient webClient = new WebClient())
			{
				try
				{
					text = webClient.DownloadString(urlSource + ".sha1");
				}
				catch (Exception ex)
				{
					Log.WriteException(ex, string.Concat(new string[]
					{
						"Failed to retrieve latest checksum for: ",
						targetFile,
						" from ",
						urlSource,
						".sha1"
					}));
					throw ex;
				}
			}
			if (string.IsNullOrEmpty(text))
			{
				throw new Exception("Invalid or empty latest checksum from: " + urlSource);
			}
			if (fileSHA != text)
			{
				this._callback.UpdateLauncherProgress(10);
				this._callback.UpdateLauncherText("Downloading latest game files...");
				using (WebClient webClient2 = new WebClient())
				{
					try
					{
						object obj = new object();
						object obj2 = obj;
						lock (obj2)
						{
							webClient2.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.DownloadProgressCallback);
							webClient2.DownloadFileCompleted += new AsyncCompletedEventHandler(this.HandleDownloadComplete);
							webClient2.DownloadFileAsync(new Uri(urlSource), targetFile, obj);
							Monitor.Wait(obj);
						}
					}
					catch (Exception expr_11F)
					{
						Log.WriteException(expr_11F, "Failed to download latest from: " + urlSource + " for " + targetFile);
						throw expr_11F;
					}
				}
				fileSHA = this.GetFileSHA1(targetFile);
				if (fileSHA != text)
				{
					throw new Exception(string.Concat(new string[]
					{
						"Could not update local file ",
						targetFile,
						" to match remote ",
						urlSource,
						" checksum."
					}));
				}
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x000039EC File Offset: 0x00001BEC
		private void DoGameFilesUpdate()
		{
			this._callback.UpdateLauncherText("Updating to latest game files...");
			this.UpdateGameFile("https://files.cheatbreaker.com/gamefiles/alpha.patch", CheatBreakerGlobals.GetCBExePath(), 10, 30);
			this.UpdateGameFile("https://files.cheatbreaker.com/gamefiles/client.patch", CheatBreakerGlobals.GetClientPatchPath(), 30, 50);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003A28 File Offset: 0x00001C28
		public void Launch(ILauncherCallback callback, MinecraftAccount selectedAccount, string serverAddress)
		{
			this._callback = callback;
			callback.UpdateLaunching(true);
			Task arg_6A_0 = Task.Run(delegate
			{
				try
				{
					Log.Write("Launch initialized...");
					callback.UpdateLauncherText("");
					callback.UpdateLauncherProgress(0);
					GameFilesUpdater gameFilesUpdater = new GameFilesUpdater();
					gameFilesUpdater.DoUpdate(callback, CheatBreakerGlobals.GetSingleton().Downloader);
					this.DoGameFilesUpdate();
					callback.UpdateLauncherProgress(50);
					callback.UpdateLauncherText("Checking game...");
					CheatBreakerGlobals.GetSingleton().Downloader.WaitForQueue();
					callback.UpdateLauncherProgress(75);
					callback.UpdateLauncherText("Launching game...");
					string launchArguments = this.GetLaunchArguments("1.7.10", selectedAccount, gameFilesUpdater.Libraries, serverAddress);
					if (!new FileCertVerify(CheatBreakerGlobals.GetCBExePath()).IsSignedByCheatBreaker())
					{
						MessageBox.Show("Game client is not signed by the CheatBreaker certificate.", "", MessageBoxButton.OK, MessageBoxImage.Hand);
						throw new Exception("Game client is not signed by the CheatBreaker certificate.");
					}
					using (Process process = new Process())
					{
						StringDictionary environmentVariables = process.StartInfo.EnvironmentVariables;
						environmentVariables["PATH"] = environmentVariables["PATH"] + ";" + CheatBreakerGlobals.GetJreDir() + "bin/server/";
						process.StartInfo.FileName = CheatBreakerGlobals.GetCBExePath();
						process.StartInfo.Arguments = launchArguments;
						process.StartInfo.UseShellExecute = false;
						process.StartInfo.RedirectStandardOutput = true;
						process.StartInfo.RedirectStandardError = true;
						process.StartInfo.Verb = "runas";
						process.StartInfo.WorkingDirectory = CheatBreakerGlobals.GetRealMinecraftDir();
						process.OutputDataReceived += new DataReceivedEventHandler(CheatBreakerGlobals.CaptureOutput);
						if (Launcher.IsCheatBreakerClientAlreadyRunning())
						{
							callback.UpdateLauncherText("Game is already running!");
							callback.UpdateGameRunning(true);
							callback.UpdateLaunching(false);
						}
						else
						{
							process.Start();
							process.BeginOutputReadLine();
							callback.UpdateLauncherText("Game running...");
							callback.UpdateLauncherProgress(100);
							callback.UpdateGameRunning(true);
							process.WaitForExit();
						}
					}
				}
				catch (Exception arg_216_0)
				{
					Log.WriteException(arg_216_0, "Failed launching.");
				}
				finally
				{
					callback.UpdateGameRunning(false);
					callback.UpdateLaunching(false);
				}
			});
			Action<Task> arg_6A_1;
			if ((arg_6A_1 = Launcher.<>c.<>9__16_1) == null)
			{
				arg_6A_1 = (Launcher.<>c.<>9__16_1 = new Action<Task>(Launcher.<>c.<>9.<Launch>b__16_1));
			}
			arg_6A_0.ContinueWith(arg_6A_1);
		}

		// Token: 0x04000035 RID: 53
		private static Launcher _instance;

		// Token: 0x04000036 RID: 54
		private static object _lockObject = new object();

		// Token: 0x04000037 RID: 55
		private ILauncherCallback _callback;

		// Token: 0x04000038 RID: 56
		private string _lastLaunchError = string.Empty;

		// Token: 0x04000039 RID: 57
		private int _minProgress;

		// Token: 0x0400003A RID: 58
		private int _maxProgress = 100;

		// Token: 0x0200005A RID: 90
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000242 RID: 578 RVA: 0x00009644 File Offset: 0x00007844
			internal void <Launch>b__16_1(Task t)
			{
				Launcher.<>c__DisplayClass16_1 <>c__DisplayClass16_ = new Launcher.<>c__DisplayClass16_1();
				<>c__DisplayClass16_.t = t;
				if (<>c__DisplayClass16_.t.IsFaulted)
				{
					Application.Current.Dispatcher.BeginInvoke(new Action(<>c__DisplayClass16_.<Launch>b__2), new object[0]);
				}
			}

			// Token: 0x0400012C RID: 300
			public static readonly Launcher.<>c <>9 = new Launcher.<>c();

			// Token: 0x0400012D RID: 301
			public static Action<Task> <>9__16_1;
		}
	}
}
