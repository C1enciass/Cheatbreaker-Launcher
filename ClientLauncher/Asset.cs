﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000006 RID: 6
	internal class Asset
	{
		// Token: 0x0600000E RID: 14 RVA: 0x000024D2 File Offset: 0x000006D2
		public Asset(string dest, string url)
		{
			this.dest = dest;
			this.url = url;
		}

		// Token: 0x04000008 RID: 8
		public string dest;

		// Token: 0x04000009 RID: 9
		public string url;
	}
}
