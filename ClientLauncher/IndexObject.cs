﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000008 RID: 8
	internal class IndexObject
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000013 RID: 19 RVA: 0x00002501 File Offset: 0x00000701
		// (set) Token: 0x06000014 RID: 20 RVA: 0x00002509 File Offset: 0x00000709
		public string hash
		{
			get;
			set;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00002512 File Offset: 0x00000712
		// (set) Token: 0x06000016 RID: 22 RVA: 0x0000251A File Offset: 0x0000071A
		public string size
		{
			get;
			set;
		}
	}
}
