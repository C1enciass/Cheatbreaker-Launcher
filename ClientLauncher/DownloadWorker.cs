﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace ClientLauncher
{
	// Token: 0x02000026 RID: 38
	internal class DownloadWorker
	{
		// Token: 0x0600014E RID: 334 RVA: 0x000061B0 File Offset: 0x000043B0
		public DownloadWorker(Downloader downloader, DownloadTask task)
		{
			this._downloader = downloader;
			this._targetFile = task._targetFile;
			this._sourceUrl = task._sourceUrl;
			this._extract = task._extract;
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00006224 File Offset: 0x00004424
		public void DownloadASync()
		{
			this._downloader.IsDownloading = true;
			this._webClient = new WebClient();
			this._webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.DownloadProgressCallback);
			this._webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(this.DownloadCompletedCallback);
			this._webClient.DownloadFileAsync(new Uri(this._sourceUrl), this._targetFile);
		}

		// Token: 0x06000150 RID: 336 RVA: 0x00006294 File Offset: 0x00004494
		public void DownloadSync()
		{
			this._downloader.IsDownloading = true;
			this._webClient = new WebClient();
			Uri address = new Uri(this._sourceUrl);
			this._webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.DownloadProgressCallback);
			this._downloader.SetProgressTextThreadSafe("Downloading " + this._downloader.CurrentTaskName);
			this._webClient.DownloadFile(address, this._targetFile);
			if (this._extract)
			{
				this._downloader.SetProgressPercentageThreadSafe(0);
				ZipFile.ExtractToDirectory(this._targetFile, Directory.GetParent(this._targetFile).ToString());
				Utility.TryFileDelete(this._targetFile);
				this._downloader.NotifyDownloadCompleted();
			}
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00006354 File Offset: 0x00004554
		private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
		{
			this._downloader.SetProgressTextThreadSafe(string.Format("Downloading {0} ({1}/{2})", this._downloader.CurrentTaskName, this.SizeSuffix(e.BytesReceived), this.SizeSuffix(e.TotalBytesToReceive)));
			this._downloader.SetProgressPercentageThreadSafe(e.ProgressPercentage);
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000063AC File Offset: 0x000045AC
		private void DownloadCompletedCallback(object sender, AsyncCompletedEventArgs e)
		{
			if (e.Error != null)
			{
				Log.WriteException(e.Error, "Trying to download: " + this._sourceUrl);
				Utility.TryFileDelete(this._targetFile);
			}
			if (this._extract)
			{
				this._downloader.SetProgressTextThreadSafe("Extracting files");
				ZipFile.ExtractToDirectory(this._targetFile, Directory.GetParent(this._targetFile).ToString());
				Utility.TryFileDelete(this._targetFile);
			}
			this._downloader.NotifyDownloadCompleted();
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00006434 File Offset: 0x00004634
		private string SizeSuffix(long value)
		{
			if (value < 0L)
			{
				return "-" + this.SizeSuffix(-value);
			}
			int num = 0;
			decimal num2 = value;
			while (Math.Round(num2 / 1024m) >= decimal.One)
			{
				num2 /= 1024m;
				num++;
			}
			return string.Format("{0:n1} {1}", num2, this.SizeSuffixes[num]);
		}

		// Token: 0x04000097 RID: 151
		private Downloader _downloader;

		// Token: 0x04000098 RID: 152
		private WebClient _webClient;

		// Token: 0x04000099 RID: 153
		private string _targetFile;

		// Token: 0x0400009A RID: 154
		private string _sourceUrl;

		// Token: 0x0400009B RID: 155
		private bool _extract;

		// Token: 0x0400009C RID: 156
		private string[] SizeSuffixes = new string[]
		{
			"bytes",
			"KB",
			"MB",
			"GB",
			"TB"
		};
	}
}
