﻿using System;
using System.Reflection;

namespace ClientLauncher
{
	// Token: 0x0200000A RID: 10
	internal static class CheatBreakerConstants
	{
		// Token: 0x04000012 RID: 18
		public static bool NO_UPDATE = false;

		// Token: 0x04000013 RID: 19
		public static bool FORCE_UPDATE = false;

		// Token: 0x04000014 RID: 20
		public static bool DEBUG = false;

		// Token: 0x04000015 RID: 21
		public const string NEWS_FEED_URL = "https://files.cheatbreaker.com/news_feed.xml";

		// Token: 0x04000016 RID: 22
		public static string CURRENT_LAUNCHER_PATH = Assembly.GetExecutingAssembly().Location;

		// Token: 0x04000017 RID: 23
		public const string VersionNumber = "1.0.9";
	}
}
