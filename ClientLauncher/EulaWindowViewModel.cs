﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Input;

namespace ClientLauncher
{
	// Token: 0x02000018 RID: 24
	public class EulaWindowViewModel
	{
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000085 RID: 133 RVA: 0x00003E80 File Offset: 0x00002080
		public string EulaText
		{
			get
			{
				string result;
				try
				{
					using (Stream manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ClientLauncher.Resources.Text.EulaText.txt"))
					{
						using (StreamReader streamReader = new StreamReader(manifestResourceStream))
						{
							result = streamReader.ReadToEnd();
						}
					}
				}
				catch
				{
					throw new Exception("Failed to load EULA text.");
				}
				return result;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000086 RID: 134 RVA: 0x00003EF8 File Offset: 0x000020F8
		// (set) Token: 0x06000087 RID: 135 RVA: 0x00003F00 File Offset: 0x00002100
		public ICommand AgreeCommand
		{
			get;
			internal set;
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000088 RID: 136 RVA: 0x00003F09 File Offset: 0x00002109
		// (set) Token: 0x06000089 RID: 137 RVA: 0x00003F11 File Offset: 0x00002111
		public ICommand DisagreeCommand
		{
			get;
			internal set;
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003F1C File Offset: 0x0000211C
		public EulaWindowViewModel(EulaWindow view)
		{
			this._view = view;
			this.AgreeCommand = new RelayCommand(new Action<object>(this.AgreeCommandExecuted), new Predicate<object>(this.CanAgreeCommandExecute));
			this.DisagreeCommand = new RelayCommand(new Action<object>(this.DisagreeCommandExecuted));
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003F70 File Offset: 0x00002170
		private void AgreeCommandExecuted(object param)
		{
			try
			{
				CheatBreakerSettings.GetInstance().SetEulaAccepted();
				WindowManager.AdvancePastEula();
			}
			catch
			{
			}
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00003FA4 File Offset: 0x000021A4
		private bool CanAgreeCommandExecute(object param)
		{
			try
			{
				double verticalOffset = this._view.EulaTextBox.VerticalOffset;
				double viewportHeight = this._view.EulaTextBox.ViewportHeight;
				double extentHeight = this._view.EulaTextBox.ExtentHeight;
				if (verticalOffset != 0.0 && verticalOffset + viewportHeight == extentHeight)
				{
					return true;
				}
			}
			catch
			{
			}
			return false;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00004014 File Offset: 0x00002214
		private void DisagreeCommandExecuted(object param)
		{
			try
			{
				App.Singleton.Quit();
			}
			catch
			{
			}
		}

		// Token: 0x04000042 RID: 66
		private EulaWindow _view;
	}
}
