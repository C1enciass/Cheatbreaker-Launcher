﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ClientLauncher
{
	// Token: 0x02000032 RID: 50
	public class LoginPasswordBox : TextBox
	{
		// Token: 0x060001A8 RID: 424 RVA: 0x00007330 File Offset: 0x00005530
		static LoginPasswordBox()
		{
			FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(LoginPasswordBox), new FrameworkPropertyMetadata(typeof(LoginPasswordBox)));
		}

		// Token: 0x060001AA RID: 426 RVA: 0x000073DD File Offset: 0x000055DD
		protected override void OnTextChanged(TextChangedEventArgs e)
		{
			base.OnTextChanged(e);
			this.HasText = (base.Text.Length != 0);
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001AB RID: 427 RVA: 0x000073FA File Offset: 0x000055FA
		// (set) Token: 0x060001AC RID: 428 RVA: 0x0000740C File Offset: 0x0000560C
		public string LabelText
		{
			get
			{
				return (string)base.GetValue(LoginPasswordBox.LabelTextProperty);
			}
			set
			{
				base.SetValue(LoginPasswordBox.LabelTextProperty, value);
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001AD RID: 429 RVA: 0x0000741A File Offset: 0x0000561A
		// (set) Token: 0x060001AE RID: 430 RVA: 0x0000742C File Offset: 0x0000562C
		public Brush LabelTextColor
		{
			get
			{
				return (Brush)base.GetValue(LoginPasswordBox.LabelTextColorProperty);
			}
			set
			{
				base.SetValue(LoginPasswordBox.LabelTextColorProperty, value);
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001AF RID: 431 RVA: 0x0000743A File Offset: 0x0000563A
		// (set) Token: 0x060001B0 RID: 432 RVA: 0x0000744C File Offset: 0x0000564C
		public bool HasText
		{
			get
			{
				return (bool)base.GetValue(LoginPasswordBox.HasTextProperty);
			}
			private set
			{
				base.SetValue(LoginPasswordBox.HasTextPropertyKey, value);
			}
		}

		// Token: 0x040000BE RID: 190
		public static DependencyProperty LabelTextProperty = DependencyProperty.Register("LabelText", typeof(string), typeof(LoginPasswordBox));

		// Token: 0x040000BF RID: 191
		public static DependencyProperty LabelTextColorProperty = DependencyProperty.Register("LabelTextColor", typeof(Brush), typeof(LoginPasswordBox));

		// Token: 0x040000C0 RID: 192
		private static DependencyPropertyKey HasTextPropertyKey = DependencyProperty.RegisterReadOnly("HasText", typeof(bool), typeof(LoginPasswordBox), new PropertyMetadata());

		// Token: 0x040000C1 RID: 193
		public static DependencyProperty HasTextProperty = LoginPasswordBox.HasTextPropertyKey.DependencyProperty;
	}
}
