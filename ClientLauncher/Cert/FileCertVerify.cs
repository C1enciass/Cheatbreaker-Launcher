﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace ClientLauncher.Cert
{
	// Token: 0x0200004E RID: 78
	internal class FileCertVerify
	{
		// Token: 0x06000213 RID: 531 RVA: 0x0000883C File Offset: 0x00006A3C
		public FileCertVerify(string filePath)
		{
			this._path = filePath;
			try
			{
				X509Certificate certificate = X509Certificate.CreateFromSignedFile(filePath);
				this._certificate = new X509Certificate2(certificate);
				if (!this._certificate.Verify())
				{
					return;
				}
				this._hasCert = true;
			}
			catch (CryptographicException arg_3A_0)
			{
				CheatBreakerGlobals.Debug(arg_3A_0.ToString());
				CheatBreakerGlobals.Debug("No certificate for file " + filePath);
				return;
			}
			if (new X509Chain
			{
				ChainPolicy = 
				{
					RevocationFlag = X509RevocationFlag.ExcludeRoot,
					RevocationMode = X509RevocationMode.Online,
					UrlRetrievalTimeout = new TimeSpan(0, 1, 0),
					VerificationFlags = X509VerificationFlags.NoFlag
				}
			}.Build(this._certificate))
			{
				this._verifiedCert = true;
			}
		}

		// Token: 0x06000214 RID: 532 RVA: 0x00008900 File Offset: 0x00006B00
		private bool HasCertificate()
		{
			return this._hasCert;
		}

		// Token: 0x06000215 RID: 533 RVA: 0x00008908 File Offset: 0x00006B08
		private bool HasValidCertificate()
		{
			return this._verifiedCert;
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00008910 File Offset: 0x00006B10
		public bool IsSignedByCheatBreaker()
		{
			if (!this.HasValidCertificate())
			{
				return false;
			}
			string text = BitConverter.ToString(this._certificate.GetPublicKey()).Replace("-", string.Empty);
			bool expr_66 = this._verifiedCert && this._certificate.SubjectName.Name.Equals(FileCertVerify.CB_CERT_SUBJECT) && text.ToLower().Equals(FileCertVerify.CB_PUBLIC_CERT.ToLower());
			if (expr_66)
			{
				CheatBreakerGlobals.Debug("File: " + this._path + " has CB cert");
			}
			return expr_66;
		}

		// Token: 0x04000105 RID: 261
		private string _path;

		// Token: 0x04000106 RID: 262
		private X509Certificate2 _certificate;

		// Token: 0x04000107 RID: 263
		private bool _hasCert;

		// Token: 0x04000108 RID: 264
		private bool _verifiedCert;

		// Token: 0x04000109 RID: 265
		private static string CB_PUBLIC_CERT = "3082010A0282010100BD0CA87802195495F1F10483CE78A3F0AECE7E2B627615D885C0F9B1DDFDE3076FA97466208ED6BBCEBF06F454BEE9F85920C9EA238C683BDDACCE6CFA2DD783E3010674AA82AE821A544ADE00897C071201C8159C077B9B28F7B3BA4EF45F937B0D7F24AD6D117DACE8DA1642D0C6FD97D5FA55EF242A7D573B0891D86EC9B70EDDE0B4BC30BA0EA27EBCE8F0ED1EE9C304A8F8C6A04ACCA3D640579A61C6F8C8ABA2706D469B21FFC34CD0B68F9556749A15355D31BCA8985E3D8A48B2D56FA712D3F412F7D0F97CADD7CFDB63CB4546EC5D91F9850D63BEEA96AA90DCB5252FF5B27077F85179C47D03A2834EA7ADC79B49BC3DB3533C3D2D5FE893CF75730203010001";

		// Token: 0x0400010A RID: 266
		private static string CB_CERT_SUBJECT = "CN=\"CheatBreaker, LLC\", O=\"CheatBreaker, LLC\", L=Santa Clarita, S=California, C=US";
	}
}
