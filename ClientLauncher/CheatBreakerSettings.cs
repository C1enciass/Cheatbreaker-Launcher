﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ClientLauncher
{
	// Token: 0x02000021 RID: 33
	[DataContract]
	public class CheatBreakerSettings : INotifyPropertyChanged
	{
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000119 RID: 281 RVA: 0x000058D1 File Offset: 0x00003AD1
		// (set) Token: 0x0600011A RID: 282 RVA: 0x000058D9 File Offset: 0x00003AD9
		[DataMember]
		public int EulaAccepted
		{
			get
			{
				return this._eulaAccepted;
			}
			set
			{
				if (this._eulaAccepted != value)
				{
					this._eulaAccepted = value;
					this.NotifyPropertyChanged("EulaAccepted");
				}
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600011B RID: 283 RVA: 0x000058F6 File Offset: 0x00003AF6
		// (set) Token: 0x0600011C RID: 284 RVA: 0x000058FE File Offset: 0x00003AFE
		[DataMember]
		public int RAMSetting
		{
			get
			{
				return this._ramSetting;
			}
			set
			{
				if (this._ramSetting != value)
				{
					this._ramSetting = value;
					this.NotifyPropertyChanged("RAMSetting");
				}
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600011D RID: 285 RVA: 0x0000591B File Offset: 0x00003B1B
		// (set) Token: 0x0600011E RID: 286 RVA: 0x00005923 File Offset: 0x00003B23
		[DataMember]
		public int UpdateBehavior
		{
			get
			{
				return this._updateBehavior;
			}
			set
			{
				if (this._updateBehavior != value)
				{
					this._updateBehavior = value;
					this.NotifyPropertyChanged("UpdateBehavior");
				}
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00005940 File Offset: 0x00003B40
		// (set) Token: 0x06000120 RID: 288 RVA: 0x00005948 File Offset: 0x00003B48
		[DataMember]
		public int LaunchBehavior
		{
			get
			{
				return this._launchBehavior;
			}
			set
			{
				if (this._launchBehavior != value)
				{
					this._launchBehavior = value;
					this.NotifyPropertyChanged("LaunchBehavior");
				}
			}
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00005968 File Offset: 0x00003B68
		public static CheatBreakerSettings GetInstance()
		{
			if (CheatBreakerSettings._instance == null)
			{
				object lockObject = CheatBreakerSettings._lockObject;
				lock (lockObject)
				{
					if (CheatBreakerSettings._instance == null)
					{
						CheatBreakerSettings.Load();
					}
				}
			}
			return CheatBreakerSettings._instance;
		}

		// Token: 0x06000122 RID: 290 RVA: 0x000059BC File Offset: 0x00003BBC
		public static string GetSettingsPath()
		{
			return Path.Combine(CheatBreakerGlobals.GetRootDir(), "settings.xml");
		}

		// Token: 0x06000123 RID: 291 RVA: 0x000059CD File Offset: 0x00003BCD
		internal void Save()
		{
			CheatBreakerSettings._serializer.Save(this, CheatBreakerSettings.GetSettingsPath());
		}

		// Token: 0x06000124 RID: 292 RVA: 0x000059DF File Offset: 0x00003BDF
		private static void Load()
		{
			CheatBreakerSettings._instance = CheatBreakerSettings._serializer.Load(CheatBreakerSettings.GetSettingsPath());
			if (CheatBreakerSettings._instance == null)
			{
				CheatBreakerSettings._instance = new CheatBreakerSettings();
			}
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00005A06 File Offset: 0x00003C06
		public bool IsEulaAccepted()
		{
			return this.EulaAccepted >= 1;
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00005A14 File Offset: 0x00003C14
		public void SetEulaAccepted()
		{
			this.EulaAccepted = 1;
			this.Save();
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000127 RID: 295 RVA: 0x00005A24 File Offset: 0x00003C24
		// (remove) Token: 0x06000128 RID: 296 RVA: 0x00005A5C File Offset: 0x00003C5C
		[method: CompilerGenerated]
		[CompilerGenerated]
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x06000129 RID: 297 RVA: 0x00005A91 File Offset: 0x00003C91
		protected void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x04000081 RID: 129
		private static CheatBreakerSettings _instance;

		// Token: 0x04000082 RID: 130
		private static CheatBreakerSerializer<CheatBreakerSettings> _serializer = new CheatBreakerSerializer<CheatBreakerSettings>();

		// Token: 0x04000083 RID: 131
		private const string SETTINGS_FILE_NAME = "settings.xml";

		// Token: 0x04000084 RID: 132
		private int _eulaAccepted;

		// Token: 0x04000085 RID: 133
		private int _ramSetting = 1024;

		// Token: 0x04000086 RID: 134
		private int _updateBehavior;

		// Token: 0x04000087 RID: 135
		private int _launchBehavior;

		// Token: 0x04000088 RID: 136
		private static object _lockObject = new object();
	}
}
