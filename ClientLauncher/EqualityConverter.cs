﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ClientLauncher
{
	// Token: 0x0200000E RID: 14
	public class EqualityConverter : IMultiValueConverter
	{
		// Token: 0x06000055 RID: 85 RVA: 0x0000306E File Offset: 0x0000126E
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Length < 2)
			{
				return false;
			}
			return values[0].Equals(values[1]);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000308D File Offset: 0x0000128D
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
