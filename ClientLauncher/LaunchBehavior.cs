﻿using System;

namespace ClientLauncher
{
	// Token: 0x0200001F RID: 31
	internal enum LaunchBehavior
	{
		// Token: 0x0400007B RID: 123
		HideLauncher,
		// Token: 0x0400007C RID: 124
		CloseLauncher,
		// Token: 0x0400007D RID: 125
		KeepLauncherOpen
	}
}
