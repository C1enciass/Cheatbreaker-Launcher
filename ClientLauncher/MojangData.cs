﻿using System;
using ClientLauncher.MojangAuth;

namespace ClientLauncher
{
	// Token: 0x0200001C RID: 28
	public class MojangData
	{
		// Token: 0x06000104 RID: 260 RVA: 0x00005580 File Offset: 0x00003780
		public static MojangData GetSingleton()
		{
			if (MojangData._instance == null)
			{
				object lockObject = MojangData._lockObject;
				lock (lockObject)
				{
					if (MojangData._instance == null)
					{
						MojangData._instance = new MojangData();
					}
				}
			}
			return MojangData._instance;
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000105 RID: 261 RVA: 0x000055D8 File Offset: 0x000037D8
		// (set) Token: 0x06000106 RID: 262 RVA: 0x000055E0 File Offset: 0x000037E0
		public MojangProfiles LauncherProfiles
		{
			get
			{
				return this._launcherProfiles;
			}
			private set
			{
				this._launcherProfiles = value;
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000055E9 File Offset: 0x000037E9
		private MojangData()
		{
			this.ReloadProfiles();
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00005602 File Offset: 0x00003802
		public void ReloadProfiles()
		{
			this.LauncherProfiles = new MojangProfiles();
		}

		// Token: 0x04000072 RID: 114
		private static MojangData _instance;

		// Token: 0x04000073 RID: 115
		private static object _lockObject = new object();

		// Token: 0x04000074 RID: 116
		private MojangProfiles _launcherProfiles = new MojangProfiles();
	}
}
