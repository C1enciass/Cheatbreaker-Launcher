﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace ClientLauncher
{
	// Token: 0x02000028 RID: 40
	internal class FileHash
	{
		// Token: 0x06000157 RID: 343 RVA: 0x00006610 File Offset: 0x00004810
		public static string sha1(string f)
		{
			string result = "";
			using (FileStream fileStream = File.OpenRead(f))
			{
				using (SHA1Managed sHA1Managed = new SHA1Managed())
				{
					result = BitConverter.ToString(sHA1Managed.ComputeHash(fileStream)).Replace("-", string.Empty);
				}
			}
			return result;
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00006680 File Offset: 0x00004880
		public static string md5(string filename)
		{
			string result;
			using (MD5 mD = MD5.Create())
			{
				using (FileStream fileStream = File.OpenRead(filename))
				{
					string arg_33_0 = BitConverter.ToString(mD.ComputeHash(fileStream)).ToLower().Replace("-", string.Empty);
					fileStream.Close();
					result = arg_33_0;
				}
			}
			return result;
		}

		// Token: 0x06000159 RID: 345 RVA: 0x000066F4 File Offset: 0x000048F4
		public static string sha256(string file)
		{
			string result;
			using (FileStream fileStream = File.OpenRead(file))
			{
				result = BitConverter.ToString(new SHA256Managed().ComputeHash(fileStream)).Replace("-", string.Empty).ToLower();
			}
			return result;
		}
	}
}
