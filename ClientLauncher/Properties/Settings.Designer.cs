﻿using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace ClientLauncher.Properties
{
	// Token: 0x02000035 RID: 53
	[CompilerGenerated, GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
	internal sealed partial class Settings : ApplicationSettingsBase
	{
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x000076F6 File Offset: 0x000058F6
		public static Settings Default
		{
			get
			{
				return Settings.defaultInstance;
			}
		}

		// Token: 0x040000CB RID: 203
		private static Settings defaultInstance = (Settings)SettingsBase.Synchronized(new Settings());
	}
}
