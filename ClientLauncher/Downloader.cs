﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;

namespace ClientLauncher
{
	// Token: 0x02000024 RID: 36
	public class Downloader : INotifyPropertyChanged
	{
		// Token: 0x06000134 RID: 308 RVA: 0x00005D6C File Offset: 0x00003F6C
		public static Downloader GetSingleton()
		{
			if (Downloader._instance == null)
			{
				object lockObject = Downloader._lockObject;
				lock (lockObject)
				{
					if (Downloader._instance == null)
					{
						Downloader._instance = new Downloader();
					}
				}
			}
			return Downloader._instance;
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000135 RID: 309 RVA: 0x00005DC4 File Offset: 0x00003FC4
		public ConcurrentQueue<DownloadTask> DownloadQueue
		{
			get
			{
				return this._downloadQueue;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000136 RID: 310 RVA: 0x00005DCC File Offset: 0x00003FCC
		// (set) Token: 0x06000137 RID: 311 RVA: 0x00005DD4 File Offset: 0x00003FD4
		public string CurrentTaskName
		{
			get
			{
				return this._currentTaskName;
			}
			set
			{
				if (this._currentTaskName != value)
				{
					this._currentTaskName = value;
					this.NotifyPropertyChanged("CurrentTaskName");
				}
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000138 RID: 312 RVA: 0x00005DF6 File Offset: 0x00003FF6
		// (set) Token: 0x06000139 RID: 313 RVA: 0x00005DFE File Offset: 0x00003FFE
		public bool IsDownloading
		{
			get
			{
				return this._isDownloading;
			}
			set
			{
				if (this._isDownloading != value)
				{
					this._isDownloading = value;
					this.NotifyPropertyChanged("IsDownloading");
				}
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x0600013A RID: 314 RVA: 0x00005E1B File Offset: 0x0000401B
		// (set) Token: 0x0600013B RID: 315 RVA: 0x00005E23 File Offset: 0x00004023
		public string ProgressText
		{
			get
			{
				return this._progressText;
			}
			set
			{
				if (this._progressText != value)
				{
					this._progressText = value;
					this.NotifyPropertyChanged("ProgressText");
				}
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00005E45 File Offset: 0x00004045
		// (set) Token: 0x0600013D RID: 317 RVA: 0x00005E4D File Offset: 0x0000404D
		public int ProgressPercentage
		{
			get
			{
				return this._progressPercentage;
			}
			set
			{
				if (this._progressPercentage != value)
				{
					this._progressPercentage = value;
					this.NotifyPropertyChanged("ProgressPercentage");
				}
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00005E6A File Offset: 0x0000406A
		private Downloader()
		{
			this._downloadQueue = new ConcurrentQueue<DownloadTask>();
			this._downloadThread = new Thread(new ThreadStart(this.DownloadWork));
			this._downloadThread.Start();
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00005EAA File Offset: 0x000040AA
		public void AddToQueue(string taskName, string dest, string url, bool extract)
		{
			this._downloadQueue.Enqueue(new DownloadTask(taskName, dest, url, extract));
		}

		// Token: 0x06000140 RID: 320 RVA: 0x00005EC4 File Offset: 0x000040C4
		public void Download(string name, string dest, string url, bool extract)
		{
			object lockObject = Downloader._lockObject;
			lock (lockObject)
			{
				DownloadTask task = new DownloadTask(name, dest, url, extract);
				this.WaitForDownload();
				new DownloadWorker(this, task).DownloadSync();
			}
			this.NotifyDownloadCompleted();
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00005F20 File Offset: 0x00004120
		private void DownloadWork()
		{
			while (!App.Singleton.ShouldQuit)
			{
				Thread.Sleep(10);
				if (!this._downloadQueue.IsEmpty)
				{
					object lockObject = Downloader._lockObject;
					lock (lockObject)
					{
						DownloadTask downloadTask;
						if (!this._isDownloading && this._downloadQueue.TryDequeue(out downloadTask))
						{
							this.SetCurrentTaskNameThreadSafe(downloadTask._taskName);
							new DownloadWorker(this, downloadTask).DownloadASync();
						}
					}
				}
			}
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00005FAC File Offset: 0x000041AC
		public void SetCurrentTaskNameThreadSafe(string taskName)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(delegate
			{
				this.CurrentTaskName = taskName;
			}), new object[0]);
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00005FF0 File Offset: 0x000041F0
		public void SetProgressTextThreadSafe(string progressText)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(delegate
			{
				this.ProgressText = progressText;
			}), new object[0]);
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00006034 File Offset: 0x00004234
		public void SetProgressPercentageThreadSafe(int progressPercentage)
		{
			Application.Current.Dispatcher.BeginInvoke(new Action(delegate
			{
				this.ProgressPercentage = progressPercentage;
			}), new object[0]);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00006077 File Offset: 0x00004277
		public void Reset()
		{
			this.SetProgressPercentageThreadSafe(0);
			this.SetCurrentTaskNameThreadSafe(string.Empty);
			this._isDownloading = false;
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006092 File Offset: 0x00004292
		public void NotifyDownloadCompleted()
		{
			this.Reset();
		}

		// Token: 0x06000147 RID: 327 RVA: 0x0000609A File Offset: 0x0000429A
		public void WaitForDownload()
		{
			Thread.Sleep(50);
			while (this._isDownloading && !App.Singleton.ShouldQuit)
			{
				Thread.Sleep(10);
			}
		}

		// Token: 0x06000148 RID: 328 RVA: 0x000060C0 File Offset: 0x000042C0
		public void WaitForQueue()
		{
			Thread.Sleep(50);
			while (!this._downloadQueue.IsEmpty && !App.Singleton.ShouldQuit)
			{
				Thread.Sleep(10);
			}
			this.WaitForDownload();
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000149 RID: 329 RVA: 0x000060F4 File Offset: 0x000042F4
		// (remove) Token: 0x0600014A RID: 330 RVA: 0x0000612C File Offset: 0x0000432C
		[method: CompilerGenerated]
		[CompilerGenerated]
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x0600014B RID: 331 RVA: 0x00006161 File Offset: 0x00004361
		public void NotifyPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x0400008A RID: 138
		private static Downloader _instance;

		// Token: 0x0400008B RID: 139
		private static object _lockObject = new object();

		// Token: 0x0400008C RID: 140
		private ConcurrentQueue<DownloadTask> _downloadQueue;

		// Token: 0x0400008D RID: 141
		private Thread _downloadThread;

		// Token: 0x0400008E RID: 142
		private string _currentTaskName;

		// Token: 0x0400008F RID: 143
		private bool _isDownloading;

		// Token: 0x04000090 RID: 144
		private string _progressText = string.Empty;

		// Token: 0x04000091 RID: 145
		private int _progressPercentage;
	}
}
