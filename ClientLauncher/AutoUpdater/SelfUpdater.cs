﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ClientLauncher.Cert;

namespace ClientLauncher.AutoUpdater
{
	// Token: 0x0200004F RID: 79
	internal class SelfUpdater
	{
		// Token: 0x06000218 RID: 536 RVA: 0x000089B8 File Offset: 0x00006BB8
		public static SelfUpdater GetSingleton()
		{
			if (SelfUpdater._instance == null)
			{
				object lockObject = SelfUpdater._lockObject;
				lock (lockObject)
				{
					if (SelfUpdater._instance == null)
					{
						SelfUpdater._instance = new SelfUpdater();
					}
				}
			}
			return SelfUpdater._instance;
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00002344 File Offset: 0x00000544
		private SelfUpdater()
		{
		}

		// Token: 0x0600021A RID: 538 RVA: 0x000029D8 File Offset: 0x00000BD8
		public void checkSignedFiles()
		{
		}

		// Token: 0x0600021B RID: 539 RVA: 0x00008A10 File Offset: 0x00006C10
		private void DownloadFile(string urlSource, string targetFile)
		{
			try
			{
				object obj = new object();
				object obj2 = obj;
				lock (obj2)
				{
					using (WebClient webClient = new WebClient())
					{
						webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.Client_DownloadProgressChanged);
						webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(this.Client_DownloadFileCompleted);
						webClient.DownloadFileAsync(new Uri(urlSource), targetFile, obj);
						Monitor.Wait(obj);
					}
				}
			}
			catch (Exception expr_6B)
			{
				Log.WriteException(expr_6B, "Failed to download file information from: " + urlSource + " for " + targetFile);
				throw expr_6B;
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x00008AC8 File Offset: 0x00006CC8
		private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			this.NotifyProgress(null, 100);
			object userState = e.UserState;
			lock (userState)
			{
				Monitor.Pulse(e.UserState);
			}
		}

		// Token: 0x0600021D RID: 541 RVA: 0x00008B18 File Offset: 0x00006D18
		private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			this.NotifyProgress(null, Math.Min(100, Math.Max(0, e.ProgressPercentage)));
		}

		// Token: 0x0600021E RID: 542 RVA: 0x00008B34 File Offset: 0x00006D34
		private string DownloadFileAsString(string urlSource)
		{
			string result = null;
			try
			{
				using (WebClient webClient = new WebClient())
				{
					result = webClient.DownloadString(new Uri(urlSource));
				}
			}
			catch (Exception expr_23)
			{
				Log.WriteException(expr_23, "Failed to download file string from: " + urlSource);
				throw expr_23;
			}
			return result;
		}

		// Token: 0x0600021F RID: 543 RVA: 0x00008B94 File Offset: 0x00006D94
		private string GetCurrentExecutableHash()
		{
			string result = null;
			try
			{
				result = Sha1.GetSha1Hash(CheatBreakerConstants.CURRENT_LAUNCHER_PATH);
			}
			catch (Exception expr_0F)
			{
				Log.WriteException(expr_0F, "Failed to get current executable hash.");
				throw expr_0F;
			}
			return result;
		}

		// Token: 0x06000220 RID: 544 RVA: 0x00008BD0 File Offset: 0x00006DD0
		private void DownloadAndInstallUpdate(string downloadUrl)
		{
			this.NotifyProgress("Downloading update...", 0);
			long num = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
			string text = Directory.GetParent(CheatBreakerConstants.CURRENT_LAUNCHER_PATH) + "\\" + SelfUpdater.TEMPORARY_UPDATE_NAME_PREFIX + num.ToString() + ".exe";
			this.DownloadFile(downloadUrl, text);
			this.NotifyProgress("Verifying downloaded file...", -1);
			if (!new FileCertVerify(text).IsSignedByCheatBreaker())
			{
				MessageBox.Show("Downloaded update file does not have a CheatBreaker certificate.", "", MessageBoxButton.OK, MessageBoxImage.Hand);
				throw new Exception("Downloaded update file does not have a CheatBreaker certificate.");
			}
			this.NotifyProgress("Running update...", -1);
			new Process
			{
				StartInfo = 
				{
					FileName = text,
					Verb = "runas"
				}
			}.Start();
			Dispatcher arg_FB_0 = Application.Current.Dispatcher;
			Action arg_FB_1;
			if ((arg_FB_1 = SelfUpdater.<>c.<>9__15_0) == null)
			{
				arg_FB_1 = (SelfUpdater.<>c.<>9__15_0 = new Action(SelfUpdater.<>c.<>9.<DownloadAndInstallUpdate>b__15_0));
			}
			arg_FB_0.BeginInvoke(arg_FB_1, new object[0]);
		}

		// Token: 0x06000221 RID: 545 RVA: 0x00008CDE File Offset: 0x00006EDE
		private void NotifyProgress(string text, int progress)
		{
			if (this.UpdateProgress != null)
			{
				Application.Current.Dispatcher.BeginInvoke(this.UpdateProgress, new object[]
				{
					text,
					progress
				});
			}
		}

		// Token: 0x06000222 RID: 546 RVA: 0x00008D11 File Offset: 0x00006F11
		public void CheckForUpdateAsync()
		{
			Task arg_30_0 = Task.Run(delegate
			{
				try
				{
					this.NotifyProgress("Deleting old update files...", -1);
					try
					{
						foreach (string current in Directory.EnumerateFiles(Directory.GetParent(CheatBreakerConstants.CURRENT_LAUNCHER_PATH).ToString(), SelfUpdater.TEMPORARY_UPDATE_NAME_PREFIX + "*.exe"))
						{
							try
							{
								Utility.TryFileDelete(current);
							}
							catch
							{
							}
						}
					}
					catch
					{
					}
					this.NotifyProgress("Checking current version information...", -1);
					string currentExecutableHash = this.GetCurrentExecutableHash();
					string text = null;
					try
					{
						this.NotifyProgress("Checking latest version information...", -1);
						text = this.DownloadFileAsString(SelfUpdater.CHECK_VERSION_URL);
					}
					catch (Exception arg_97_0)
					{
						Log.WriteException(arg_97_0, "Could not download latest version information.");
					}
					if (text != null)
					{
						string text2 = null;
						if (text != currentExecutableHash)
						{
							text2 = SelfUpdater.UPDATE_FILE_URL;
						}
						if (!string.IsNullOrEmpty(text2))
						{
							this.DownloadAndInstallUpdate(text2);
						}
					}
				}
				catch (Exception ex)
				{
					Log.WriteException(ex, "Failed to check for update.");
					MessageBox.Show("Failed to verify CheatBreaker launcher is up to date! The launcher will now close.\n\nTechnical details:\n" + ex.StackTrace.ToString(), "", MessageBoxButton.OK, MessageBoxImage.Hand);
					Dispatcher arg_128_0 = Application.Current.Dispatcher;
					Action arg_128_1;
					if ((arg_128_1 = SelfUpdater.<>c.<>9__17_1) == null)
					{
						arg_128_1 = (SelfUpdater.<>c.<>9__17_1 = new Action(SelfUpdater.<>c.<>9.<CheckForUpdateAsync>b__17_1));
					}
					arg_128_0.BeginInvoke(arg_128_1, new object[0]);
					return;
				}
				if (this.SelfUpdateComplete != null)
				{
					Application.Current.Dispatcher.BeginInvoke(this.SelfUpdateComplete, new object[0]);
				}
			});
			Action<Task> arg_30_1;
			if ((arg_30_1 = SelfUpdater.<>c.<>9__17_2) == null)
			{
				arg_30_1 = (SelfUpdater.<>c.<>9__17_2 = new Action<Task>(SelfUpdater.<>c.<>9.<CheckForUpdateAsync>b__17_2));
			}
			arg_30_0.ContinueWith(arg_30_1);
		}

		// Token: 0x0400010B RID: 267
		private static SelfUpdater _instance;

		// Token: 0x0400010C RID: 268
		private static object _lockObject = new object();

		// Token: 0x0400010D RID: 269
		private static string CHECK_VERSION_URL = "https://files.cheatbreaker.com/gamefiles/launcher.patch.sha1";

		// Token: 0x0400010E RID: 270
		private static string UPDATE_FILE_URL = "https://files.cheatbreaker.com/gamefiles/launcher.patch";

		// Token: 0x0400010F RID: 271
		private static string TEMPORARY_UPDATE_NAME_PREFIX = "launcherupdate_";

		// Token: 0x04000110 RID: 272
		public Action SelfUpdateComplete;

		// Token: 0x04000111 RID: 273
		public Action<string, int> UpdateProgress;

		// Token: 0x02000065 RID: 101
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600025A RID: 602 RVA: 0x00009777 File Offset: 0x00007977
			internal void <DownloadAndInstallUpdate>b__15_0()
			{
				Application.Current.Shutdown();
			}

			// Token: 0x0600025B RID: 603 RVA: 0x00009777 File Offset: 0x00007977
			internal void <CheckForUpdateAsync>b__17_1()
			{
				Application.Current.Shutdown();
			}

			// Token: 0x0600025C RID: 604 RVA: 0x00009784 File Offset: 0x00007984
			internal void <CheckForUpdateAsync>b__17_2(Task t)
			{
				SelfUpdater.<>c__DisplayClass17_0 <>c__DisplayClass17_ = new SelfUpdater.<>c__DisplayClass17_0();
				<>c__DisplayClass17_.t = t;
				if (<>c__DisplayClass17_.t.IsFaulted)
				{
					Application.Current.Dispatcher.BeginInvoke(new Action(<>c__DisplayClass17_.<CheckForUpdateAsync>b__3), new object[0]);
				}
			}

			// Token: 0x0400013E RID: 318
			public static readonly SelfUpdater.<>c <>9 = new SelfUpdater.<>c();

			// Token: 0x0400013F RID: 319
			public static Action <>9__15_0;

			// Token: 0x04000140 RID: 320
			public static Action <>9__17_1;

			// Token: 0x04000141 RID: 321
			public static Action<Task> <>9__17_2;
		}
	}
}
