﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ClientLauncher
{
	// Token: 0x0200002F RID: 47
	public static class BrowserBehavior
	{
		// Token: 0x06000190 RID: 400 RVA: 0x00006EE4 File Offset: 0x000050E4
		[AttachedPropertyBrowsableForType(typeof(WebBrowser))]
		public static NewsEntry GetHtml(WebBrowser d)
		{
			return d.GetValue(BrowserBehavior.HtmlProperty) as NewsEntry;
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00006EF6 File Offset: 0x000050F6
		public static void SetHtml(WebBrowser d, NewsEntry value)
		{
			d.SetValue(BrowserBehavior.HtmlProperty, value);
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00006F04 File Offset: 0x00005104
		private static void OnHtmlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			WebBrowser webBrowser = d as WebBrowser;
			if (webBrowser != null)
			{
				NewsEntry newsEntry = e.NewValue as NewsEntry;
				if (newsEntry != null && webBrowser.IsLoaded)
				{
					string text = "";
					text += "<html><head><style>body{background-color:#f5f5f5;font-size:18px;font-family:\"Sans\"}.title{font-size:22px;font-weight:bold;margin-bottom:5px;}.authorDiv{margin-bottom:5px;}.dateStamp{}.author{color:#c00000;}</style></head><body>";
					text = text + "<div class=\"title\">" + newsEntry.Title + "</div>";
					text = string.Concat(new string[]
					{
						text,
						"<div class=\"authorDiv\"><span class=\"dateStamp\">",
						newsEntry.Date,
						"</span> - <span class=\"author\">",
						newsEntry.Author,
						"</span></div>"
					});
					text = text + "<div class=\"contents\">" + newsEntry.Contents + "</div>";
					text += "</body></html>";
					webBrowser.NavigateToString(text);
				}
			}
		}

		// Token: 0x040000B6 RID: 182
		public static readonly DependencyProperty HtmlProperty = DependencyProperty.RegisterAttached("Html", typeof(NewsEntry), typeof(BrowserBehavior), new FrameworkPropertyMetadata(new PropertyChangedCallback(BrowserBehavior.OnHtmlChanged)));
	}
}
