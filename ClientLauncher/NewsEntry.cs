﻿using System;

namespace ClientLauncher
{
	// Token: 0x0200002E RID: 46
	public class NewsEntry
	{
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000185 RID: 389 RVA: 0x00006E8F File Offset: 0x0000508F
		// (set) Token: 0x06000186 RID: 390 RVA: 0x00006E97 File Offset: 0x00005097
		public string Title
		{
			get;
			set;
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00006EA0 File Offset: 0x000050A0
		// (set) Token: 0x06000188 RID: 392 RVA: 0x00006EA8 File Offset: 0x000050A8
		public string Date
		{
			get;
			set;
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000189 RID: 393 RVA: 0x00006EB1 File Offset: 0x000050B1
		// (set) Token: 0x0600018A RID: 394 RVA: 0x00006EB9 File Offset: 0x000050B9
		public string Author
		{
			get;
			set;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600018B RID: 395 RVA: 0x00006EC2 File Offset: 0x000050C2
		// (set) Token: 0x0600018C RID: 396 RVA: 0x00006ECA File Offset: 0x000050CA
		public string Contents
		{
			get;
			set;
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00006ED3 File Offset: 0x000050D3
		// (set) Token: 0x0600018E RID: 398 RVA: 0x00006EDB File Offset: 0x000050DB
		public string AuthorImageURL
		{
			get;
			set;
		}
	}
}
