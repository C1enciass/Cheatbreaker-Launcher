﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;

namespace ClientLauncher
{
	// Token: 0x0200000C RID: 12
	internal class RssXmlReader : XmlTextReader
	{
		// Token: 0x06000045 RID: 69 RVA: 0x00002E7B File Offset: 0x0000107B
		public RssXmlReader(Stream s) : base(s)
		{
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002E84 File Offset: 0x00001084
		public RssXmlReader(string inputUri) : base(inputUri)
		{
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002E90 File Offset: 0x00001090
		public override void ReadStartElement()
		{
			if (string.Equals(base.NamespaceURI, string.Empty, StringComparison.InvariantCultureIgnoreCase) && (string.Equals(base.LocalName, "lastBuildDate", StringComparison.InvariantCultureIgnoreCase) || string.Equals(base.LocalName, "pubDate", StringComparison.InvariantCultureIgnoreCase)))
			{
				this._readingDate = true;
			}
			base.ReadStartElement();
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002EE3 File Offset: 0x000010E3
		public override void ReadEndElement()
		{
			if (this._readingDate)
			{
				this._readingDate = false;
			}
			base.ReadEndElement();
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002EFC File Offset: 0x000010FC
		public override string ReadString()
		{
			if (this._readingDate)
			{
				string s = base.ReadString();
				DateTime dateTime;
				if (!DateTime.TryParse(s, out dateTime))
				{
					dateTime = DateTime.ParseExact(s, "ddd MMM dd HH:mm:ss Z yyyy", CultureInfo.InvariantCulture);
				}
				return dateTime.ToUniversalTime().ToString("R", CultureInfo.InvariantCulture);
			}
			return base.ReadString();
		}

		// Token: 0x04000023 RID: 35
		private bool _readingDate;

		// Token: 0x04000024 RID: 36
		private const string CUSTOM_UTC_DATETIME_FORMAT = "ddd MMM dd HH:mm:ss Z yyyy";
	}
}
