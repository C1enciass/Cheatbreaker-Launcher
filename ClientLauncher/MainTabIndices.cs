﻿using System;

namespace ClientLauncher
{
	// Token: 0x0200002D RID: 45
	public enum MainTabIndices
	{
		// Token: 0x040000AC RID: 172
		Home,
		// Token: 0x040000AD RID: 173
		Servers,
		// Token: 0x040000AE RID: 174
		Settings,
		// Token: 0x040000AF RID: 175
		About,
		// Token: 0x040000B0 RID: 176
		LaunchGame
	}
}
