﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace ClientLauncher
{
	// Token: 0x0200002C RID: 44
	public partial class App : Application
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000171 RID: 369 RVA: 0x00006B00 File Offset: 0x00004D00
		// (set) Token: 0x06000172 RID: 370 RVA: 0x00006B07 File Offset: 0x00004D07
		public static App Singleton
		{
			get;
			set;
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00006B0F File Offset: 0x00004D0F
		// (set) Token: 0x06000174 RID: 372 RVA: 0x00006B17 File Offset: 0x00004D17
		public bool ShouldQuit
		{
			get;
			set;
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00006B20 File Offset: 0x00004D20
		public App()
		{
			App.Singleton = this;
			this.ShouldQuit = false;
			base.Dispatcher.UnhandledException += new DispatcherUnhandledExceptionEventHandler(this.OnDispatcherUnhandledException);
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00006B4C File Offset: 0x00004D4C
		public void Quit()
		{
			WindowManager.Shutdown();
			this.ShouldQuit = true;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00006B5C File Offset: 0x00004D5C
		private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			try
			{
				string expr_44 = string.Concat(new string[]
				{
					"An unhandled exception occurred: ",
					e.Exception.Message,
					Environment.NewLine,
					Environment.NewLine,
					e.Exception.StackTrace.ToString()
				});
				Log.Write(expr_44);
				MessageBox.Show(expr_44, "CheatBreaker Error", MessageBoxButton.OK, MessageBoxImage.Hand);
				base.Shutdown();
			}
			catch
			{
			}
			e.Handled = true;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000306B File Offset: 0x0000126B
		private bool RemoteCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00006BE4 File Offset: 0x00004DE4
		internal static byte ReadByte(byte[] buffer)
		{
			byte arg_13_0 = buffer[App._offset];
			App._offset++;
			return arg_13_0;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00006BFC File Offset: 0x00004DFC
		internal static byte[] Read(byte[] buffer, int length)
		{
			byte[] array = new byte[length];
			Array.Copy(buffer, App._offset, array, 0, length);
			App._offset += length;
			return array;
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00006C2C File Offset: 0x00004E2C
		internal static int ReadVarInt(byte[] buffer)
		{
			int num = 0;
			int num2 = 0;
			int num3;
			while (((num3 = (int)App.ReadByte(buffer)) & 128) == 128)
			{
				num |= (num3 & 127) << num2++ * 7;
				if (num2 > 5)
				{
					throw new IOException("This VarInt is an imposter!");
				}
			}
			return num | (num3 & 127) << num2 * 7;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x000029D8 File Offset: 0x00000BD8
		private void MinecraftPingCallback(MinecraftPingResult result)
		{
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00006C84 File Offset: 0x00004E84
		private void App_Startup(object sender, StartupEventArgs e)
		{
			ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(this.RemoteCertificateValidation);
			try
			{
				Utility.TryCreateDirectory(CheatBreakerGlobals.GetRootDir());
				Utility.TryCreateDirectory(CheatBreakerGlobals.GetRootDir() + "logs");
				Log.Open(CheatBreakerGlobals.GetRootDir() + "logs\\launcher_log.txt");
				Log.Write(string.Format("Starting up (Version: {0})", "1.0.9"));
			}
			catch
			{
			}
			if (!this.CheckOS())
			{
				base.Shutdown();
				return;
			}
			if (this.CheckIfAlreadyRunning())
			{
				base.Shutdown();
				return;
			}
			WindowManager.Init();
			if (!CheatBreakerSettings.GetInstance().IsEulaAccepted())
			{
				Log.Write("EULA not yet accepted. Showing EULA.");
				WindowManager.ShowEula();
				return;
			}
			WindowManager.AdvancePastEula();
		}

		// Token: 0x0600017E RID: 382 RVA: 0x0000306B File Offset: 0x0000126B
		private bool CheckOS()
		{
			return true;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00006D44 File Offset: 0x00004F44
		private void App_Exit(object sender, ExitEventArgs e)
		{
			CheatBreakerSettings.GetInstance().Save();
			this.DisposeInstanceMutex();
			Log.Write(string.Format("Shutting down (Version: {0})", "1.0.9"));
			Log.Close();
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00006D70 File Offset: 0x00004F70
		private void DisposeInstanceMutex()
		{
			try
			{
				if (App._mutex != null)
				{
					App._mutex.Dispose();
					App._mutex = null;
				}
			}
			catch (Exception arg_1E_0)
			{
				Log.WriteException(arg_1E_0, "Error disposing instance mutex.");
			}
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00006DB4 File Offset: 0x00004FB4
		private bool CheckIfAlreadyRunning()
		{
			bool flag = false;
			try
			{
				App._mutex = new Mutex(true, "CheatBreakerLauncherInstance");
				if (App._mutex.WaitOne(TimeSpan.Zero, true))
				{
					App._mutex.ReleaseMutex();
				}
				else
				{
					flag = true;
					this.DisposeInstanceMutex();
				}
			}
			catch (Exception arg_3F_0)
			{
				Log.WriteException(arg_3F_0, "Error creating instance mutex.");
			}
			if (flag)
			{
				MessageBox.Show("Only one instance of the CheatBreaker launcher application should be run at a time.");
				return true;
			}
			return false;
		}

		// Token: 0x040000A8 RID: 168
		private static Mutex _mutex;

		// Token: 0x040000A9 RID: 169
		private static int _offset;
	}
}
