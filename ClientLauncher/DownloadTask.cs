﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000025 RID: 37
	public class DownloadTask
	{
		// Token: 0x0600014D RID: 333 RVA: 0x00006189 File Offset: 0x00004389
		public DownloadTask(string taskName, string targetFile, string sourceUrl, bool shouldExtract)
		{
			this._taskName = taskName;
			this._targetFile = targetFile;
			this._sourceUrl = sourceUrl;
			this._extract = shouldExtract;
		}

		// Token: 0x04000093 RID: 147
		public string _targetFile;

		// Token: 0x04000094 RID: 148
		public string _sourceUrl;

		// Token: 0x04000095 RID: 149
		public string _taskName;

		// Token: 0x04000096 RID: 150
		public bool _extract;
	}
}
