﻿using System;

namespace ClientLauncher
{
	// Token: 0x02000017 RID: 23
	public static class Log
	{
		// Token: 0x06000080 RID: 128 RVA: 0x00003CE0 File Offset: 0x00001EE0
		public static void Open(string filePath)
		{
			object @lock = Log._lock;
			lock (@lock)
			{
				if (Log._log == null)
				{
					Log._log = new FileLogger(filePath);
				}
			}
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003D2C File Offset: 0x00001F2C
		public static void Close()
		{
			object @lock = Log._lock;
			lock (@lock)
			{
				if (Log._log != null)
				{
					Log._log.Close();
					Log._log = null;
				}
			}
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00003D7C File Offset: 0x00001F7C
		public static void Write(string text)
		{
			object @lock = Log._lock;
			lock (@lock)
			{
				if (Log._log != null)
				{
					Log._log.Write(text);
				}
			}
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00003DCC File Offset: 0x00001FCC
		public static void WriteException(Exception ex, string text)
		{
			object @lock = Log._lock;
			lock (@lock)
			{
				if (Log._log != null)
				{
					Log._log.Write(text);
					Log._log.Write("Exception: " + ex.ToString());
					if (ex.StackTrace != null)
					{
						Log._log.Write("Stack: " + ex.StackTrace.ToString());
					}
					else
					{
						Log._log.Write("No stack trace!");
					}
				}
			}
		}

		// Token: 0x04000040 RID: 64
		private static FileLogger _log = null;

		// Token: 0x04000041 RID: 65
		private static object _lock = new object();
	}
}
