﻿using System;
using System.Threading;

namespace ClientLauncher
{
	// Token: 0x02000027 RID: 39
	internal class FileCheck
	{
		// Token: 0x06000154 RID: 340 RVA: 0x000064B4 File Offset: 0x000046B4
		public static bool CheckFiles(string version)
		{
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressTextThreadSafe("Checking files");
			CheatBreakerGlobals.GetSingleton().Downloader.SetProgressPercentageThreadSafe(0);
			string text = CheatBreakerGlobals.GetVersionDir(version) + version + ".jar";
			string hashOfDir = FolderHash.GetHashOfDir(CheatBreakerGlobals.GetJreDir());
			string hashOfDir2 = FolderHash.GetHashOfDir(CheatBreakerGlobals.GetLibraryDir(version));
			string hashOfDir3 = FolderHash.GetHashOfDir(CheatBreakerGlobals.GetNativesDir(version));
			string text2 = FileHash.sha256(text);
			if (FileCheck.jreHash != hashOfDir)
			{
				Utility.TryDirectoryDelete(CheatBreakerGlobals.GetJreDir(), true);
				Thread.Sleep(500);
				CheatBreakerGlobals.GetSingleton().Downloader.NotifyDownloadCompleted();
				CheatBreakerGlobals.GetSingleton().DownloadJRE();
				CheatBreakerGlobals.GetSingleton().Downloader.WaitForQueue();
				return false;
			}
			if (hashOfDir2 != FileCheck.libsHash || hashOfDir3 != FileCheck.nativesHash)
			{
				Utility.TryDirectoryDelete(CheatBreakerGlobals.GetLibraryDir(version), true);
				Utility.TryDirectoryDelete(CheatBreakerGlobals.GetNativesDir(version), true);
				Thread.Sleep(500);
				CheatBreakerGlobals.GetSingleton().Downloader.NotifyDownloadCompleted();
				return false;
			}
			if (text2 != FileCheck.minecraftJarHash)
			{
				Console.WriteLine(text2);
				Console.WriteLine(FileCheck.minecraftJarHash);
				Console.WriteLine(text);
				return false;
			}
			return true;
		}

		// Token: 0x0400009D RID: 157
		private static string jreHash = "A1C25CBDDE53CE3B1F35A4A12E01A83638066AE5";

		// Token: 0x0400009E RID: 158
		private static string libsHash = "E365500BC456940433A82CC26CB492E03ED4123F";

		// Token: 0x0400009F RID: 159
		private static string nativesHash = "063731D83366D25465430832DEF2BEF4AD2F66C9";

		// Token: 0x040000A0 RID: 160
		private static string minecraftJarHash = "a4fc2284657544e0f4bcc964f927c2fda3e3a205178ed1d5d58883aaf9780cce";
	}
}
